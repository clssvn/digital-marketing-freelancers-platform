﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FreelancePlatform.Models;
using FreelancePlatform.Resources;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FreelancePlatform.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SkillsController : ControllerBase
    {
        private topmContext _context;
        private readonly IMapper _mapper;


        public SkillsController(topmContext context, UserManager<User> userManager, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;

        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<NicheResource>>> GetSkills()
        {
            var skills = await _context.Skill.ToListAsync();
            var skillsResources = _mapper.Map<IEnumerable<Skill>, IEnumerable<SkillResource>>(skills).ToList();

            return Ok(skillsResources);
        }


    }
}