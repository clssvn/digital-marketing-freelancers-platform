﻿using System;
using System.Collections.Generic;

namespace FreelancePlatform.Models
{
    public partial class Project
    {
        public Project()
        {
            ImageFile = new HashSet<ImageFile>();
        }

        public int IdProject { get; set; }
        public string Name { get; set; }
        public DateTime? AddedDate { get; set; }
        public int IdFreelancer { get; set; }
        public int IdKlient { get; set; }
        public string Goals { get; set; }
        public string Description { get; set; }
        public string Result { get; set; }
        public decimal? Price { get; set; }
        public int NicheId { get; set; }
        public int CategoryId { get; set; }
        public int? ReviewId { get; set; }

        public virtual Category Category { get; set; }
        public virtual Freelancer IdFreelancerNavigation { get; set; }
        public virtual Klient IdKlientNavigation { get; set; }
        public virtual Niche Niche { get; set; }
        public virtual Review Review { get; set; }
        public virtual ICollection<ImageFile> ImageFile { get; set; }
    }
}
