import React, { Component } from "react";
import { MainHeader, UserRoutingHeader } from "../../header";
import FreelancerCard from "../../freelancer-card";
import { Link } from "react-router-dom";
import { fetchFreelancers } from "../../../actions";
import { compose } from "redux";
import { connect } from "react-redux";
import FreelanceAppService from "../../../services/freelance-app-service";
import { withFreelanceAppService } from "../../hoc";
import './freelancers-search.scss'
import Loading from "../../loading/loading";
import Error from "../../error/error";

const UsersList = ({ freelancers }) => {

  return Array.from(freelancers).map((freelancer) => {
      return (
        <li key={freelancer.id}>
          <Link to={`/freelancer/id${freelancer.id}`}>
            <FreelancerCard freelancer={{freelancer, loading: false}} />
          </Link>
        </li>
    );
  });
};

class FreelancerSearch extends Component {
  componentDidMount() {
    this.props.fetchFreelancers();
  }

  render() {
    const { freelancers, loading, error } = this.props.freelancers;

    if (loading) {
      return <Loading />;
    }

    if(error){
      return <Error />;
  }

    return (
      <>
        <UserRoutingHeader />
        <UsersList freelancers={freelancers}/>
      </>
      )
      
  }
}

const mapDispatchToProps = (dispatch, { freelanceAppService }) => {
  return {
    fetchFreelancers: () => fetchFreelancers(freelanceAppService, dispatch),
  };
};

const mapStateToProps = ({ freelancers }) => {
  return { freelancers };
};

export default compose(
  withFreelanceAppService(),
  connect(mapStateToProps, mapDispatchToProps)
)(FreelancerSearch);
