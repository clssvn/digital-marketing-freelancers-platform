﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace FreelancePlatform.Models
{
    public partial class User : IdentityUser<int>
    {
        override
        public string Email { get; set; }
        public string Pass { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }

        public virtual UserInfo UserInfo { get; set; }
        public virtual Contact Contact { get; set; }
    }
}
