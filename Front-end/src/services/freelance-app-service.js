import configData from "../config.json";

export default class FreelanceAppService{

    _apiBase = configData.SERVER_URL;
    token = localStorage.getItem("token");

    isAuth = (token) => {
      if (token != null){
        return(
          {
          headers: {
              'Authorization': `Bearer ${this.token}`
          }
        })
      }
    }

    deleteResource = async (url) => {
      const res = await fetch(`${this._apiBase}${url}`, 
      {
        method: "DELETE",
        headers: {
            'Authorization': `Bearer ${this.token}`
        }
      });
  
      if (!res.ok) {
        throw new Error(`Could not fetch ${url}` +
          `, received ${res.status}`)
      }
      return await res.json();
    };

    verifyGet = async (url) => {
      const res = await fetch(`${this._apiBase}${url}`, this.isAuth(this.token));
  
      if (!res.ok) {
        throw new Error(`Could not fetch ${url}` +
          `, received ${res.status}`)
      }
      return await res;
    };

    getResource = async (url) => {
        const res = await fetch(`${this._apiBase}${url}`, this.isAuth(this.token));
    
        if (!res.ok) {
          throw new Error(`Could not fetch ${url}` +
            `, received ${res.status}`)
        }
        return await res.json();
      };

      getAuthResource = async (url, token) => {
        const res = await fetch(`${this._apiBase}${url}`, {
            method: "GET",
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });
    
        if (!res.ok) {
          throw new Error(`Could not fetch ${url}` +
            `, received ${res.status}`)
        }
        return await res.json();
      };

    postResource = async (url, data) => {
        const res = await fetch (`${this._apiBase}${url}`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(data)
        });

        if (!res.ok) {
            throw new Error(`Could not fetch ${url}` +
              `, received ${res.status}`)
          }

        return await res.json();
    };

    postAuthResource = async (url, data) => {
        const res = await fetch (`${this._apiBase}${url}`, {
            method: "POST",
            headers: {
              'Authorization': `Bearer ${this.token}`,
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(data)
        });

        if (!res.ok) {
            throw new Error(`Could not fetch ${url}` +
              `, received ${res.status}`)
          }

        return await res;
    };

    postFile = async (url, data) => {
        const res = await fetch (`${this._apiBase}${url}`, {
            method: "POST",
            headers: {
              'Authorization': `Bearer ${this.token}`
            },
            body: data
        });

        if (!res.ok) {
            throw new Error(`Could not fetch ${url}` +
              `, received ${res.status}`)
          }

        return await res;
    };

      getUserDataForEdit = async () => {
        const res = await this.getAuthResource(`User/edit`, localStorage.token);
          return res;
      }

      updateUserAvatar = async (data) => {
        const body = new FormData();
        body.append("file", data);
        const res = await this.postFile(`User/edit/avatar`, body, localStorage.token);
          return res;
      }

      updateUserPass = async (data) => {
        const res = await this.postAuthResource(`User/edit/pass`, data);
          return res;
      }

      updateUserData = async (data) => {
        const res = await this.postAuthResource(`User/edit/data`, data);
          return res;
      }

      getCurrentUser = async (user) => {
        const res = await this.postResource(`Auth/signin`, user);
        return res;
      }

      getCurrentUserInfo = async () => {
          const res = await this.getAuthResource(`Auth/current`, localStorage.token);
          return res;
      }

      declineUser = async (id) => {
        const res = await this.deleteResource(`Auth/${id}`, localStorage.token);
        return res;
      }

      verifyUser = async (id) => {
        const res = await this.verifyGet(`Auth/verify/${id}`, localStorage.token);
        return res;
      }
//CLIENT
      getActiveClients = async () => {
        const res = await this.getAuthResource(`Client/active`, localStorage.token);
        return res;
      };

      getUnverifiedClients = async () => {
        const res = await this.getAuthResource(`Client/unverified`, localStorage.token);
        return res;
      };

      getClient = async (id) => {
        const res = await this.getAuthResource(`Client/${id}`, localStorage.token);
        return res;
      };
//FREELANCER
      getActiveFreelancers = async () => {
        const res = await this.getAuthResource(`Freelancers/active`, localStorage.token);
        return res;
      };

      getUnverifiedFreelancers = async () => {
        const res = await this.getAuthResource(`Freelancers/unverified`, localStorage.token);
        return res;
      };

      getFreelancer = async (id) => {
        const res = await this.getAuthResource(`Freelancers/${id}`, localStorage.token);
        return res;
      };
//REVIEWS  
      getReview = async (id) => {
        const res = await this.getAuthResource(`Reviews/${id}`, localStorage.token);
        return res;
      };
      
      getFreelancerReviews = async (id) => {
        const res = await this.getAuthResource(`Reviews/freelancer=${id}`, localStorage.token);
        return res;
      }; 

      getNewReviews = async (id) => {
        const res = await this.getResource(`Reviews/project=${id}`);
        return res;
      }; 
//CASES
      getCase = async (id) => {
        const res = await this.getAuthResource(`Cases/${id}`, localStorage.token);
        return res;
      };
      
      getUserCases = async (id) => {
        const res = await this.getAuthResource(`Cases/user=${id}`, localStorage.token);
        return res;
      }; 

      getCases = async () => {
        const res = await this.getAuthResource(`Cases`, localStorage.token);
        return res;
      };

};