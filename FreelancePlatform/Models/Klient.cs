﻿using System;
using System.Collections.Generic;

namespace FreelancePlatform.Models
{
    public partial class Klient
    {
        public Klient()
        {
            Project = new HashSet<Project>();
            Review = new HashSet<Review>();
        }

        public string Name { get; set; }
        public int CompanySize { get; set; }
        public int NicheId { get; set; }
        public int Id { get; set; }

        public virtual UserInfo IdNavigation { get; set; }
        public virtual Niche Niche { get; set; }
        public virtual ICollection<Project> Project { get; set; }
        public virtual ICollection<Review> Review { get; set; }
    }
}
