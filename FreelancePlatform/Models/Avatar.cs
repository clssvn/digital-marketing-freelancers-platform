﻿using System;
using System.Collections.Generic;

namespace FreelancePlatform.Models
{
    public partial class Avatar
    {
        public Avatar()
        {
            UserInfo = new HashSet<UserInfo>();
        }

        public int IdAvatar { get; set; }
        public string Avatarlink { get; set; }

        public virtual ICollection<UserInfo> UserInfo { get; set; }
    }
}
