import React, { Component, useState } from "react";
import {
  Container,
  Row,
  Col,
  Button,
  CloseButton,
  Image,
  Form,
  FloatingLabel,
  Modal,
} from "react-bootstrap";
import "./edit-profile.scss";
import { compose } from "redux";
import { connect } from "react-redux";
import {
  fetchUserEditData,
  fetchUserNewData,
  fetchUserNewAvatar,
  fetchUserNewPass,
  fetchCurrentUserInfo,
} from "../../../actions";
import { withFreelanceAppService } from "../../hoc";
import { Link } from "react-router-dom";
import AsyncSelect from "react-select/async";
import makeAnimated from "react-select/animated";
import Loading from "../../loading/loading";
import Error from "../../error/error";
import { toast } from "react-toastify";
import configData from "../../../config.json";
import { validPassword } from "../../../regex.js";

const _apiBase = configData.SERVER_URL;
const animatedComponents = makeAnimated();

const contactTypes = [
  { id: 1, name: "Instagram" },
  { id: 2, name: "Facebook" },
  { id: 3, name: "LinkedIn" },
  { id: 4, name: "WhatsApp" },
  { id: 5, name: "E-mail" },
];

const GoBack = () => {
  return (
    <Link to="/">
      <CloseButton />
    </Link>
  );
};

const ChangePassModal = ({ show, onHide, fetchUserNewPass, updated }) => {
  const [currentPass, setCurrentPass] = useState("");
  const [newPass, setNewPass] = useState("");
  const [validated, setValidated] = useState(false);
  const [btnDisable, setBtnDisable] = useState(false);

  const handleSubmit = (event) => {
    setBtnDisable(true);
    const form = event.currentTarget;
    event.preventDefault();

    if (form.checkValidity() === false) {
      event.stopPropagation();
      setBtnDisable(false);
      toast.error("Please, fill all fields");
      return;
    }

    if (!validPassword.test(newPass)) {
      toast.error(
        `Password must contain at least 8 characters but no longer than 36 characters. At least one digit, one letter and one nonalphanumeric character.`
      );
      setBtnDisable(false);
      return;
    }

    setValidated(true);

    event.preventDefault();
    var data = { current: currentPass, new: newPass };
    fetchUserNewPass(data);
    //setBtnDisable(false);
    updated();
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Change password
        </Modal.Title>
      </Modal.Header>
      <Form noValidate validated={validated} onSubmit={handleSubmit}>
        <Modal.Body>
          <Form.Group className="mb-3" controlId="formOldPassword">
            <Form.Label>Current password</Form.Label>
            <Form.Control
              required
              type="password"
              placeholder="Password"
              onChange={(event) => setCurrentPass(event.target.value)}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formNewPassword">
            <Form.Label>New password</Form.Label>
            <Form.Control
              required
              type="password"
              placeholder="Password"
              onChange={(event) => setNewPass(event.target.value)}
            />
          </Form.Group>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={onHide}>Close</Button>
          <Button type="submit" disabled={btnDisable}>
            Submit
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
};

const EditSkills = ({ role, skills, loadOptionsSkills, setSkills }) => {
  if (role === "freelancer") {
    return (
      <>
        <h4 className="edit-title">Change your skills</h4>
        <Row className="edit-row">  
          <AsyncSelect
            className="edit-contact-type"
            cacheOptions
            components={animatedComponents}
            defaultOptions
            isMulti
            value={skills}
            loadOptions={loadOptionsSkills}
            placeholder="Skills"
            name="skills"
            getOptionLabel={(e) => e.name}
            getOptionValue={(e) => e.idSkill}
            onChange={(value) => setSkills(value)}
          />
        </Row>
      </>
    );
  } else {
    return null;
  }
};

class EditProfile extends Component {
  state = {
    showPassChange: false,
    updated: false,
    uploaded: false,
    avatar: "",
    newAvatarFile: {},
    contactType: "",
    contact: "",
    info: "",
    skills: [],
  };

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  loadOptionsSkills = async () => {
    const res = await fetch(`${_apiBase}Skills`);

    if (!res.ok) {
      throw new Error(`Could not fetch, received ${res.status}`);
    }
    return await res.json().then((result) => {
      return result;
    });
  };

  userEditDataToState = () => {
    const { userEditData } = this.props.userEditData;
    this.setState({
      uploaded: true,
      avatar: userEditData.avatar,
      contactType: userEditData.contact.type,
      contact: userEditData.contact.contact1,
      info: userEditData.info,
      skills: userEditData.skill,
    });
  };

  componentDidMount() {
    this.props.fetchUserEditData();
    this.props.fetchCurrentUserInfo();
  }

  updateAvatar = () => {
    var newAvatar = this.state.newAvatarFile;
    this.props.fetchUserNewAvatar(newAvatar);
    this.setState({ updated: true });
  };

  updateData = () => {
    const { userEditData } = this.props.userEditData;
    var data = {};

    if (
      this.state.contact !== userEditData.contact.contact1 ||
      this.state.contactType !== userEditData.contact.type
    ) {
      data = {
        ...data,
        contact: {
          type: this.state.contactType,
          contact1: this.state.contact,
        },
      };
    }

    if (this.state.info !== userEditData.info) {
      data = {
        ...data,
        info: this.state.info,
      };
    }

    if (
      JSON.stringify(this.state.skills) !== JSON.stringify(userEditData.skill)
    ) {
      var skillFreelancerResource = [];
      this.state.skills.forEach(
        (s) =>
          (skillFreelancerResource = [
            ...skillFreelancerResource,
            {
              idSkill: s.idSkill,
            },
          ])
      );

      data = {
        ...data,
        skillFreelancerResource: skillFreelancerResource,
      };
    }
    if (Object.keys(data).length !== 0) {
      this.props.fetchUserNewData(data);
      this.setState({ updated: true });
    }
  };

  render() {
    const { loading, error } = this.props.userEditData;
    const { userNewData } = this.props;

    if (loading) {
      return <Loading />;
    }

    if (error || userNewData.error) {
      toast.error("Something went wrong");
    }

    if (
      userNewData.userNewData.ok &&
      this.state.updated &&
      this.state.uploaded
    ) {
      this.props.fetchUserEditData();
      this.setState({ updated: false });
      this.setState({ uploaded: false });
    }

    if (
      userNewData.userNewData.ok &&
      this.state.updated &&
      this.state.showPassChange
    ) {
      this.setState({ updated: false });
      this.setState({ showPassChange: false });
    }

    if (!this.state.uploaded) {
      this.userEditDataToState();
    }

    return (
      <>
        <ChangePassModal
          show={this.state.showPassChange}
          onHide={() => this.setState({ showPassChange: false })}
          fetchUserNewPass={this.props.fetchUserNewPass}
          updated={() => this.setState({ updated: true })}
        />
        <Container>
          <Row className="header-edit-page">
            <Col sm={11}>
              <h2>Edit profile</h2>
            </Col>
            <Col sm={1}>
              <GoBack />
            </Col>
          </Row>

          <Row className="edit">
            <Col className="current-avatar">
              <Image
                src={this.state.avatar}
                rounded
                style={{ maxWidth: 180 }}
              />
            </Col>
            <Col className="edit-avatar">
              <Row>
                <h4>Edit avatar</h4>
              </Row>
              <Row className="edit-row file-input-row">
                <Form.Control
                  type="file"
                  accept="image/*"
                  onChange={(value) => {
                    this.setState({ newAvatarFile: value.target.files[0] });
                  }}
                />
              </Row>
              <Button
                as="input"
                type="button"
                value="Save avatar"
                onClick={this.updateAvatar}
              />
            </Col>
          </Row>

          <h4 className="edit-title">Contact</h4>

          <Row className="edit-row">
            <Col className="edit-contact-type">
              <Form.Select
                aria-label=""
                name="contactType"
                onChange={this.handleChange}
                value={this.state.contactType}
              >
                {contactTypes.map((p) => (
                  <option value={p.id}>{p.name}</option>
                ))}
              </Form.Select>
            </Col>
            
            <Col>
              <FloatingLabel
                controlId="floatingInput"
                label="Contact"
                className="mb-3"
              >
                <Form.Control
                  type="text"
                  name="contact"
                  onChange={this.handleChange}
                  value={this.state.contact}
                />
              </FloatingLabel>
            </Col>
          </Row>
          <h4 className="edit-title"> Change your bio</h4>

          <Row className="edit-row">
            <Form.Control
              className="textarea-info"
              as="textarea"
              rows={3}
              name="info"
              onChange={this.handleChange}
              value={this.state.info}
            />
          </Row>
          <EditSkills
            role={this.props.currentUser.currentUser.role}
            skills={this.state.skills}
            loadOptionsSkills={this.loadOptionsSkills}
            setSkills={(skills) => {
              this.setState({ skills: skills });
            }}
          />
          <Row className="edit-row">
          <Button
            className="edit-pass"
            as="input"
            type="button"
            value="Change password"
            onClick={() => this.setState({ showPassChange: true })}
          />
          <Button
            className="edit-cancel"
            as="input"
            type="button"
            value="Cancel changes"
            onClick={this.userEditDataToState}
          />
          <Button
            className="edit-save"
            as="input"
            type="button"
            value="Save changes"
            onClick={this.updateData}
          />
          </Row>
        </Container>
      </>
    );
  }
}

const mapDispatchToProps = (dispatch, { freelanceAppService }) => {
  return {
    fetchUserEditData: () => fetchUserEditData(freelanceAppService, dispatch),
    fetchUserNewData: (data) =>
      fetchUserNewData(freelanceAppService, dispatch, data),
    fetchUserNewAvatar: (data) =>
      fetchUserNewAvatar(freelanceAppService, dispatch, data),
    fetchUserNewPass: (data) =>
      fetchUserNewPass(freelanceAppService, dispatch, data),
    fetchCurrentUserInfo: () =>
      fetchCurrentUserInfo(freelanceAppService, dispatch),
  };
};

const mapStateToProps = ({ userEditData, userNewData, currentUser }) => {
  return { userEditData, userNewData, currentUser };
};

export default compose(
  withFreelanceAppService(),
  connect(mapStateToProps, mapDispatchToProps)
)(EditProfile);
