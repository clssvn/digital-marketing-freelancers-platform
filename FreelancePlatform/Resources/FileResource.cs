﻿using System;
using Microsoft.AspNetCore.Http;

namespace FreelancePlatform.Resources
{
    public class FileResource
    {
        public IFormFile File { get; set; }
    }
}
