﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace FreelancePlatform.Migrations
{
    public partial class DeleteAdminEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "administrator");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "administrator",
                columns: table => new
                {
                    id_user = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("administrator_pk", x => x.id_user);
                    table.ForeignKey(
                        name: "administrator_user",
                        column: x => x.id_user,
                        principalTable: "AspNetUsers",
                        principalColumn: "id_user",
                        onDelete: ReferentialAction.Cascade);
                });
        }
    }
}
