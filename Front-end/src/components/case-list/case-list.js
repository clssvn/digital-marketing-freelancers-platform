import React, { Component } from 'react'
import CaseCard from '../case-card';
import './case-list.scss';
import { Link } from 'react-router-dom';
import {
    Container,
    Row
  } from "react-bootstrap";
import Loading from '../loading/loading';
import Error from '../error/error';

class CaseList extends Component {

    freelancerId = this.props.freelancerId;

    render(){
        const { cases, loading, error } = this.props.cases;

        if(loading){
            return <Loading />;
        }
    
        if(error){
            return <Error />;
        }

        if(cases.length === 0) {
            return <h4 className="no-cases">No projects yet</h4>;
          }
        
        return(
          <Container  className="clist">
              <Row className="d-flex flex-row flex-nowrap">
            {cases.map((c) => {       
            return (
              <>
                <li key={c.idProject}>
                    <Link to={`/case/${c.idProject}`}>
                        <CaseCard project={c} />
                    </Link>
                </li>
                </>
            )
        })}
        </Row>
        </Container>
    )}
}

export default CaseList; 