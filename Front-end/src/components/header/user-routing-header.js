import React from "react";
import { Link } from "react-router-dom";
import "./header.scss";
import Logout from "./logout.js";

import {
  Nav,
  Navbar,
} from "react-bootstrap";

const EditProfile = ({ role, home }) => {
  if (role != "admin" && role != undefined && home) {
    return (
      <Nav.Item>
        <Nav.Link className="edit-profile" href="/edit-profile">
          Edit profile
        </Nav.Link>
      </Nav.Item>
    );
  } else {
    return null;
  }
};

export const UserRoutingHeader = ({ role, home }) => {
  return (
    <Navbar  expand="lg">
        <Navbar.Brand href="/">#TopM</Navbar.Brand>
       <Navbar.Toggle aria-controls="responsive-navbar-nav" /> <Navbar.Collapse id="responsive-navbar-nav">
    <Nav className="userRoutingHeader" variant="pills" justify fill>
      <Nav.Item>
        <Nav.Link className="cases" href="/cases">
          Cases
        </Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link className="freelancers" href="/freelancers">
          Freelancers
        </Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link className="clients" href="/clients">
          Clients
        </Nav.Link>
      </Nav.Item>
      <EditProfile role={role} home={home} />
      <Logout />
    </Nav>
    </Navbar.Collapse>
    </Navbar>
  );
};
