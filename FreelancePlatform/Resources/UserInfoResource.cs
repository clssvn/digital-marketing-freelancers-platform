﻿using System;
namespace FreelancePlatform.Resources
{
    public class UserInfoResource
    {
        public int Id { get; set; }
        public bool IsVerified { get; set; }
        public DateTime RegDate { get; set; }
        public string Info { get; set; }
        public int CityId { get; set; }
        public int AvatarIdAvatar { get; set; }
    }
}
