﻿using System;
using System.Collections.Generic;

namespace FreelancePlatform.Resources
{
    public class UserDataForEditResource
    {
        public string? Avatar { get; set; }
        public ContactResource Contact { get; set; }
        public string Info { get; set; }
        public ICollection<SkillResource>? Skill { get; set; }

    }
}
