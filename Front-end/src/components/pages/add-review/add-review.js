import React, { Component, useState } from "react";
import { Modal, Button, Form } from "react-bootstrap";
import FloatingLabel from "react-bootstrap/FloatingLabel";
import "./add-review.scss";
import { toast } from "react-toastify";
import configData from "../../../config.json";

const _apiBase = configData.SERVER_URL;

const postResource = async (data) => {
  const res = await fetch(`${_apiBase}Reviews`, {
    method: "POST",
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  });
  return res;
};

class MyVerticallyCenteredModal extends Component {
  state = {
    btnDisabled: false,
    content: "",
    projectId: "",
  };

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleProjectChange = (event) => {
    this.setState({
      projectId: event.target.value,
    });
  };

  handleSubmit = async (event, onHide) => {
    this.setState({ btnDisabled: true });
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
      this.setState({ btnDisabled: false });
      toast.error("Please, fill all fields");
      return;
    }

    event.preventDefault();

    var res = await postResource({
      content: this.state.content,
      projectId: this.state.projectId
    });
    if (!res.ok) {
      toast.error("Something went wrong");
    }

    onHide();
    window.location.reload(); 
  };

  render() {
  const { show, onHide, projects } = this.props;
    return (
      <Modal
        show={show}
        onHide={onHide}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header>
          <Modal.Title id="contained-modal-title-vcenter">
            Your review
          </Modal.Title>
        </Modal.Header>
        <Form
          noValidate
          validated={this.state.validated}
          onSubmit={(event) => {
            this.handleSubmit(event, onHide);
          }}
        >
          <Modal.Body>
            <FloatingLabel controlId="floatingSelect" label="Project">
              <Form.Select
                required
                name="idProject"
                onChange={this.handleProjectChange}
              >
                <option key="blankChoice" hidden value="">
                  Select project from freelancer portfolio
                </option>
                {projects.map((p) => (
                  <option value={p.idProject}>{p.name}</option>
                ))}
              </Form.Select>
            </FloatingLabel>

            <FloatingLabel controlId="floatingTextarea2" label="Your review">
              <Form.Control
                required
                className="review-textarea"
                as="textarea"
                name="content"
                style={{ height: "100px" }}
                value={this.state.content}
                onChange={this.handleChange}
              />
            </FloatingLabel>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={onHide}>Close</Button>
            <Button type="submit" disabled={this.state.btnDisabled}>
              Submit
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    );
  }
}

const AddReview = ({ projects }) => {
  const [modalShow, setModalShow] = useState(false);
  return (
    <div className="add-review-button-div">
      <Button className="add-review-button" onClick={() => setModalShow(true)}>
        Add review
      </Button>

      <MyVerticallyCenteredModal
        show={modalShow}
        onHide={() => setModalShow(false)}
        projects={projects}
      />
    </div>
  );
};

export default AddReview;
