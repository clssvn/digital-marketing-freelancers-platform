import React, { Component } from "react";
import { UserRoutingHeader } from "../../header";
import CaseCard from "../../case-card";
import { Link } from "react-router-dom";
import { fetchCases } from "../../../actions";
import { compose } from "redux";
import { connect } from "react-redux";
import FreelanceAppService from "../../../services/freelance-app-service";
import { withFreelanceAppService } from "../../hoc";
import { Container } from "react-bootstrap";
import './case-search.scss';
import Loading from "../../loading/loading";
import Error from "../../error/error";

const CasesList = ({ cases }) => {
  
  return(
    <Container className="list-cases">
{
  Array.from(cases).map((c) => {
    return (
          <Link to={`/case/${c.idProject}`}>
            <CaseCard project={c} />
          </Link>
    );
  })}
  </Container>);
};

class CasesSearch extends Component {
  componentDidMount() {
    this.props.fetchCases();
  }

  render() {
    const { cases, loading, error } = this.props.cases;
    
    if (loading) {
      return <Loading />;
    }

    if(error){
      return <Error />;
  }

    return (
      <>
        <UserRoutingHeader />
        <CasesList cases={cases}/>
      </>
      )
      
  }
}

const mapDispatchToProps = (dispatch, { freelanceAppService }) => {
  return {
    fetchCases: () => fetchCases(freelanceAppService, dispatch)
  };
};

const mapStateToProps = ({ cases }) => {
  return { cases };
};

export default compose(
  withFreelanceAppService(),
  connect(mapStateToProps, mapDispatchToProps)
)(CasesSearch);
