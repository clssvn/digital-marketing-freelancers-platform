import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { fetchCurrentUser } from "../../../actions";
import { withFreelanceAppService } from "../../hoc";
import AsyncSelect from "react-select/async";
import makeAnimated from "react-select/animated";
import { Button, Col, Form, Row } from "react-bootstrap";
import "./signup.scss";
import { MainHeader } from "../../header";
import { toast } from "react-toastify";
import configData from "../../../config.json";
import { validEmail, validPassword } from "../../../regex.js";


const _apiBase = configData.SERVER_URL;
const animatedComponents = makeAnimated();

const contactTypes = [
  { id: 1, name: "Instagram" },
  { id: 2, name: "Facebook" },
  { id: 3, name: "LinkedIn" },
  { id: 4, name: "WhatsApp" },
  { id: 5, name: "E-mail" }
];

const companySize = [
  { id: 1, name: "1 to 9 employees" },
  { id: 2, name: "10 to 49 employees" },
  { id: 3, name: "50 to 249 employees" },
  { id: 4, name: "250 employees or more" },
];

const postResource = async (data) => {
  const res = await fetch(`${_apiBase}Auth/signup/client`, {
    method: "POST",
    body: data,
  });

  return res
};

class SignupClient extends Component {
  state = {
    btnDisabled: false,
    validated1: false,
    validated2: false,

    query: "",
    step: false,

    email: "",
    name: "",
    surname: "",
    pass: "",
    avatar: {},

    city: "",
    country: "",

    info: "",

    contactId: "",
    contact: "",

    nicheId: "",
    companySize: "",
    companyName: "",
  };

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  uploadFiles = (event) => {
    this.setState({
      [event.target.name]: event.target.files[0],
    });
  };

  handleSubmit = async (event) => {
    this.setState({ btnDisabled: true });
    const form = event.currentTarget;
    if (
      form.checkValidity() === false ||
      this.state.city === "" ||
      this.state.country === "" ||
      this.state.contactId === "" ||
      this.state.nicheId === [] ||
      this.state.companySize === ""
    ) {
      event.preventDefault();
      event.stopPropagation();
      toast.error("Please, fill all required fields");
      this.setState({ btnDisabled: false });

      return;
    }

    this.setState({ validated2: true });

    event.preventDefault();

    const formData = new FormData();

    formData.append("UserSignUpResource.Email", this.state.email);
    formData.append("UserSignUpResource.name", this.state.name);
    formData.append("UserSignUpResource.surname", this.state.surname);
    formData.append("UserSignUpResource.pass", this.state.pass);
    formData.append("UserInfoSignUpResource.avatar", this.state.avatar);
    formData.append(
      "UserInfoSignUpResource.CityResource.Name",
      this.state.city
    );
    formData.append(
      "UserInfoSignUpResource.CityResource.CountryResource.Name",
      this.state.country
    );
    formData.append("UserInfoSignUpResource.Info", this.state.info);

    formData.append("KlientSignUpDataResource.nicheId", this.state.nicheId);
    formData.append(
      "KlientSignUpDataResource.companySize",
      this.state.companySize
    );
    formData.append("KlientSignUpDataResource.Name", this.state.companyName);

    formData.append(
      "UserSignUpResource.ContactResource.Type",
      this.state.contactId
    );
    formData.append(
      "UserSignUpResource.ContactResource.Contact1",
      this.state.contact
    );

    var res = await postResource(formData);
    if(res.ok){
    this.props.history.push("/signup/confirm");
    }

    if(!res.ok){
      toast.error("Something went wrong. Please, try again")
  }
  };

  loadOptionsLocation = async () => {
    if (this.state.query.trim() === "") return;

    const res = await fetch(
      `https://wft-geo-db.p.rapidapi.com/v1/geo/cities?namePrefix=${this.state.query}&sort=-population`,
      {
        method: "GET",
        headers: {
          "x-rapidapi-key":
            "83325b42d2msh989a2969abed419p11a09fjsn63352cbeb83d",
          "x-rapidapi-host": "wft-geo-db.p.rapidapi.com",
        },
      }
    );
    if (!res.ok) {
      throw new Error(`Could not fetch, received ${res.status}`);
    }
    return await res.json().then((result) => {
      return result.data;
    });
  };

  loadOptionsNiche = async () => {
    const res = await fetch(`${_apiBase}Niche`);

    if (!res.ok) {
      throw new Error(`Could not fetch, received ${res.status}`);
    }
    return await res.json().then((result) => {
      return result;
    });
  };

  loadOptionsContacts = async () => {
    return contactTypes;
  };

  loadOptionsCompanySize = async () => {
    return companySize;
  };

  changeStep = (event) => {
    event.preventDefault();
    const form = event.currentTarget;
    if (form.checkValidity() === false || this.state.nicheId === "") {
     
      event.stopPropagation();
      toast.error("Please, fill all fields");
      return;
    }

    if (!validEmail.test(this.state.email)) {
      toast.error("Please, write correct email");
      this.setState({ btnDisabled: false });
      return;
    }

    if (!validPassword.test(this.state.pass)) {
      toast.error(`Password must contain at least 8 characters but no longer than 30 characters. At least one digit, one letter and one nonalphanumeric character.`);
      this.setState({ btnDisabled: false });
      return;
    }
    
    event.preventDefault();
    this.setState({ validated1: true });
    this.setState({ step: true });
  };

  render() {
    if (!this.state.step) {
      return (
        <>
          <MainHeader />

          <div className="signin-signup-title">Apply to join us as client</div>
          <Form
            className="signup-form"
            noValidate
            validated={this.state.validated1}
            onSubmit={this.changeStep}
          >
            <Row className="signup-row">
              <Col className="signup-col">
                <Form.Control
                  required
                  type="text"
                  className="form-control"
                  name="name"
                  placeholder="Name"
                  value={this.state.name}
                  onChange={this.handleChange}
                />

                <Form.Control
                  required
                  type="text"
                  className="form-control"
                  name="surname"
                  placeholder="Surname"
                  value={this.state.surname}
                  onChange={this.handleChange}
                />

                <AsyncSelect
                  cacheOptions
                  components={animatedComponents}
                  defaultOptions
                  className="asyncselect"
                  loadOptions={this.loadOptionsNiche}
                  placeholder="Business niche"
                  getOptionLabel={(e) => e.name}
                  getOptionValue={(e) => e.idNiche}
                  onInputChange={(value) => this.setState({ query: value })}
                  onChange={(value) =>
                    this.setState({ query: "", nicheId: value.idNiche })
                  }
                />
              </Col>
              <Col className="signup-col">
                <Form.Control
                  required
                  type="text"
                  className="form-control"
                  name="companyName"
                  placeholder="Company name"
                  value={this.state.companyName}
                  onChange={this.handleChange}
                />
                <Form.Control
                  required
                  name="email"
                  placeholder="Email"
                  value={this.state.email}
                  onChange={this.handleChange}
                />
                <Form.Control
                  required
                  type="password"
                  name="pass"
                  placeholder="Password"
                  value={this.state.pass}
                  onChange={this.handleChange}
                />
                <Button type="submit" className="signup-submit">
                  Next
                </Button>
              </Col>
            </Row>
          </Form>
        </>
      );
    }

    if (this.state.step) {
      return (
        <>
          <MainHeader />

          <div className="signin-signup-title">Apply to join us as client</div>
          <Form
            className="signup-form"
            noValidate
            validated={this.state.validated2}
            onSubmit={this.handleSubmit}
          >
            <Row className="signup-row">
              <Col className="signup-col">
                <Form.Group as={Row} className="mb-3">
                  <Form.Label column sm={4}>
                    Profile photo
                  </Form.Label>
                  <Col sm={8}>
                    <Form.Control
                      required
                      name="avatar"
                      type="file"
                      accept="image/*"
                      onChange={this.uploadFiles}
                    />
                  </Col>
                </Form.Group>
                <Form.Group as={Row} className="mb-3">
                  <Form.Label column sm={4}>
                    Size of company
                  </Form.Label>
                  <Col sm={8}>
                    <AsyncSelect
                      className="asyncselect"
                      cacheOptions
                      components={animatedComponents}
                      defaultOptions
                      loadOptions={this.loadOptionsCompanySize}
                      getOptionLabel={(e) => e.name}
                      getOptionValue={(e) => e.id}
                      onChange={(value) =>
                        this.setState({ companySize: value.id })
                      }
                    />
                  </Col>
                </Form.Group>
                <Form.Group as={Row} className="mb-3">
                  <Form.Label column sm={4}>
                    Location
                  </Form.Label>
                  <Col sm={8}>
                    <AsyncSelect
                      cacheOptions
                      components={animatedComponents}
                      defaultOptions
                      loadOptions={this.loadOptionsLocation}
                      placeholder="Your city"
                      getOptionLabel={(e) => `${e.name} (${e.country})`}
                      getOptionValue={(e) => e.idLocation}
                      onInputChange={(value) => this.setState({ query: value })}
                      onChange={(value) => {
                        this.setState({
                          query: "",
                          city: value.name,
                          country: value.country,
                        });
                      }}
                    />
                  </Col>
                </Form.Group>
                <Form.Group as={Row} className="mb-3">
                  <Col className="select-contact-type" sm={4}>
                    <AsyncSelect
                      className="asyncselect"
                      cacheOptions
                      components={animatedComponents}
                      defaultOptions
                      loadOptions={this.loadOptionsContacts}
                      placeholder="Contact type"
                      getOptionLabel={(e) => e.name}
                      getOptionValue={(e) => e.id}
                      onChange={(value) =>
                        this.setState({ contactId: value.id })
                      }
                    />
                  </Col>
                  <Col sm={8}>
                    <Form.Control
                      required
                      name="contact"
                      placeholder="Contact"
                      value={this.state.contact}
                      onChange={this.handleChange}
                    />
                  </Col>
                </Form.Group>
                <Form.Group as={Row} className="mb-3">
                  <Form.Label column sm={4}>
                    About me
                  </Form.Label>
                  <Col sm={8}>
                    <Form.Control
                      required
                      as="textarea"
                      name="info"
                      placeholder="Info"
                      rows={3}
                      value={this.state.about}
                      onChange={this.handleChange}
                    />
                  </Col>
                </Form.Group>
                <Button
                  type="submit"
                  disabled={this.state.btnDisabled}
                  className="signup-submit"
                >
                  Submit
                </Button>
                <Button
                  className="signup-submit back"
                  onClick={() => this.setState({ step: false })}
                >
                  Back
                </Button>
              </Col>
            </Row>
          </Form>
        </>
      );
    }
  }
}

const mapDispatchToProps = (dispatch, { freelanceAppService }) => {
  return {
    fetchCurrentUser: (userInfo) =>
      fetchCurrentUser(freelanceAppService, dispatch, userInfo),
  };
};

export default compose(
  withFreelanceAppService(),
  connect(null, mapDispatchToProps)
)(SignupClient);
