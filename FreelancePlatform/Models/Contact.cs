﻿using System;
using System.Collections.Generic;

namespace FreelancePlatform.Models
{
    public partial class Contact
    {
        public int IdContact { get; set; }
        public string Type { get; set; }
        public string Contact1 { get; set; }
        public int UserId { get; set; }

        public virtual User User { get; set; }
    }
}
