﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FreelancePlatform.Models;
using FreelancePlatform.Resources;

namespace FreelancePlatform.Services
{
    public class ApiServices
    {
        private readonly IMapper _mapper;

        public ApiServices(IMapper mapper)
        {
            _mapper = mapper;

        }

        public ApiServices()
        {
        }

        public FreelancerResource MapFreelancer(Freelancer freelancer)
        {
            var freelancerResource = _mapper.Map<Freelancer, FreelancerResource>(freelancer);

            freelancerResource.Skill = MapFreelancerSkills(freelancer.SkillFreelancer);

            return freelancerResource;

        }

        public ICollection<SkillResource> MapFreelancerSkills(ICollection<SkillFreelancer> skillFreelancer)
        {
            var skills = new List<SkillResource>();
            skillFreelancer.ToList().ForEach(s => skills.Add(_mapper.Map<Skill, SkillResource>(s.Skill)));
            return skills;
        }

        public ICollection<SkillFreelancer> MapSkillFreelancer(ICollection<SkillFreelancerResource> skillFreelancerResource)
        {
            var skills = new List<SkillFreelancer>();
            skillFreelancerResource.ToList()
                .ForEach(s => skills.Add(_mapper.Map<SkillFreelancerResource, SkillFreelancer>(s)));
            return skills;
        }

        public string GetRegDateDiff(DateTime dateTime)
        {
            var rr = "";
            var diff = DateTime.Now.Subtract(dateTime).TotalDays;
            if (diff >= 365)
            {
                var y = Math.Round(diff / 365);
                if (y == 1)
                {
                    rr = y + " year";
                }
                else
                {
                    rr = y + " years";
                }
            }
            if (diff >= 30)
            {
                var y = Math.Round(diff / 30);
                if (y == 1)
                {
                    rr = y + " month";
                }
                else
                {
                    rr = y + " months";
                }
            }
            if (diff < 30)
            {
                var y = Math.Round(diff / 1);
                if (y == 1)
                {
                    rr = y + " day";
                }

                else
                {
                    rr = y + " days";
                }
            }

            return rr;
        }
    }
}
