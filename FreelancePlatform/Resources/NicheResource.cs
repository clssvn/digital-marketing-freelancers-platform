﻿using System;
namespace FreelancePlatform.Resources
{
    public class NicheResource
    {
        public int IdNiche { get; set; }
        public string Name { get; set; }
    }
}
