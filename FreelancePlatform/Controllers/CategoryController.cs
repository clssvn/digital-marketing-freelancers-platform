﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FreelancePlatform.Models;
using FreelancePlatform.Resources;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FreelancePlatform.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private topmContext _context;
        //private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;


        public CategoryController(topmContext context, UserManager<User> userManager, IMapper mapper)
        {
            _context = context;
            //_userManager = userManager;
            _mapper = mapper;

        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<CategoryResource>>> GetCategories()
        {
            var categories = await _context.Category.ToListAsync();
            var categoriesResources = _mapper.Map<IEnumerable<Category>, IEnumerable<CategoryResource>>(categories).ToList();

            return Ok(categoriesResources);
        }

        
    }
}