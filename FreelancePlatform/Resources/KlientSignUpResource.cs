﻿using FreelancePlatform.Models;

namespace FreelancePlatform.Resources
{
    public class KlientSignUpResource
    {
        public UserSignUpResource UserSignUpResource { get; set; }
        public UserInfoSignUpResource UserInfoSignUpResource { get; set; }
        public KlientSignUpDataResource KlientSignUpDataResource { get; set; }
    }

    public class KlientSignUpDataResource
    {
        public string Name { get; set; }
        public int CompanySize { get; set; }
        public int NicheId { get; set; }
    }
}