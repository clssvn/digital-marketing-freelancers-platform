const projectLoaded = (project) => {
  return {
    type: "FETCH_PROJECT_SUCCESS",
    payload: project,
  };
};

const projectRequested = () => {
  return {
    type: "FETCH_PROJECT_REQUEST",
  };
};

const projectError = (error) => {
  return {
    type: "FETCH_PROJECT_FAILURE",
    payload: error,
  };
};

const projectUnload = () => {
  return {
    type: "FETCH_PROJECT_UNLOAD",
  };
};

const reviewsListLoaded = (reviewsList) => {
  return {
    type: "FETCH_REVIEWS_SUCCESS",
    payload: reviewsList,
  };
};

const reviewsListRequested = () => {
  return {
    type: "FETCH_REVIEWS_REQUEST",
  };
};

const reviewsListError = (error) => {
  return {
    type: "FETCH_REVIEWS_FAILURE",
    payload: error,
  };
};

const currentUserLoaded = (currentUser) => {
  return {
    type: "FETCH_CURRENT_USER_SUCCESS",
    payload: currentUser,
  };
};

const currentUserRequested = () => {
  return {
    type: "FETCH_CURRENT_USER_REQUEST",
  };
};

const currentUserError = (error) => {
  return {
    type: "FETCH_CURRENT_USER_FAILURE",
    payload: error,
  };
};

const currentUserInfoLoaded = (currentUserInfo) => {
  return {
    type: "FETCH_CURRENT_USER_INFO_SUCCESS",
    payload: currentUserInfo,
  };
};

const currentUserInfoRequested = () => {
  return {
    type: "FETCH_CURRENT_USER_INFO_REQUEST",
  };
};

const currentUserInfoError = (error) => {
  return {
    type: "FETCH_CURRENT_USER_INFO_FAILURE",
    payload: error,
  };
};

const verifyUserRequested = () => {
  return {
    type: "VERIFY_USER_REQUEST",
  };
};

const verifyUserLoaded = (result) => {
  return {
    type: "VERIFY_USER_SUCCESS",
    payload: result,
  };
};

const verifyUserError = (error) => {
  return {
    type: "VERIFY_USER_FAILURE",
    payload: error,
  };
};

const userNewDataRequested = () => {
  return {
    type: "USER_NEW_DATA_REQUEST",
  };
};

const userNewDataLoaded = (result) => {
  return {
    type: "USER_NEW_DATA_SUCCESS",
    payload: result,
  };
};

const userNewDataError = (error) => {
  return {
    type: "USER_NEW_DATA_FAILURE",
    payload: error,
  };
};

const userEditDataRequested = () => {
  return {
    type: "USER_EDIT_DATA_REQUEST",
  };
};

const userEditDataLoaded = (result) => {
  return {
    type: "USER_EDIT_DATA_SUCCESS",
    payload: result,
  };
};

const userEditDataError = (error) => {
  return {
    type: "USER_EDIT_DATA_FAILURE",
    payload: error,
  };
};

const freelancerRequested = () => {
  return {
    type: "FETCH_FREELANCER_REQUEST",
  };
};

const freelancerLoaded = (freelancer) => {
  return {
    type: "FETCH_FREELANCER_SUCCESS",
    payload: freelancer,
  };
};

const freelancerError = (error) => {
  return {
    type: "FETCH_FREELANCER_FAILURE",
    payload: error,
  };
};

const freelancersLoaded = (freelancers) => {
  return {
    type: "FETCH_FREELANCERS_SUCCESS",
    payload: freelancers,
  };
};

const freelancersRequested = () => {
  return {
    type: "FETCH_FREELANCERS_REQUEST",
  };
};

const freelancersError = (error) => {
  return {
    type: "FETCH_FREELANCERS_FAILURE",
    payload: error,
  };
};

const clientLoaded = (client) => {
  return {
    type: "FETCH_CLIENT_SUCCESS",
    payload: client,
  };
};

const clientRequested = () => {
  return {
    type: "FETCH_CLIENT_REQUEST",
  };
};

const clientError = (error) => {
  return {
    type: "FETCH_CLIENT_FAILURE",
    payload: error,
  };
};

const clientsLoaded = (clients) => {
  return {
    type: "FETCH_CLIENTS_SUCCESS",
    payload: clients,
  };
};

const clientsRequested = () => {
  return {
    type: "FETCH_CLIENTS_REQUEST",
  };
};

const clientsError = (error) => {
  return {
    type: "FETCH_CLIENTS_FAILURE",
    payload: error,
  };
};

const casesListLoaded = (casesList) => {
  return {
    type: "FETCH_CASES_SUCCESS",
    payload: casesList,
  };
};

const casesListRequested = () => {
  return {
    type: "FETCH_CASES_REQUEST",
  };
};

const casesListError = (error) => {
  return {
    type: "FETCH_CASES_FAILURE",
    payload: error,
  };
};

const casesLoaded = (casesList) => {
  return {
    type: "FETCH_ALLCASES_SUCCESS",
    payload: casesList,
  };
};

const casesRequested = () => {
  return {
    type: "FETCH_ALLCASES_REQUEST",
  };
};

const casesError = (error) => {
  return {
    type: "FETCH_ALLCASES_FAILURE",
    payload: error,
  };
};

const fetchReviewsList = (freelanceAppService, dispatch, freelancerId) => {
  dispatch(reviewsListRequested());
  freelanceAppService
    .getFreelancerReviews(freelancerId)
    .then((data) => dispatch(reviewsListLoaded(data)))
    .catch((err) => dispatch(reviewsListError(err)));
};

const fetchProject = (freelanceAppService, dispatch, projectId) => {
  dispatch(projectRequested());

  freelanceAppService
    .getCase(projectId)
    .then((data) => dispatch(projectLoaded(data)))
    .catch((err) => dispatch(projectError(err)));
};

const fetchClient = (freelanceAppService, dispatch, userId) => {
  dispatch(clientRequested());

  freelanceAppService
    .getClient(userId)
    .then((data) => dispatch(clientLoaded(data)))
    .catch((err) => dispatch(clientError(err)));
};

const fetchCurrentUser = (freelanceAppService, dispatch, user) => {
  dispatch(currentUserRequested());

  freelanceAppService
    .getCurrentUser(user)
    .then((data) => {
      localStorage.setItem("token", data);
      dispatch(currentUserLoaded(data));
    })
    .catch((err) => dispatch(currentUserError(err)));
};

const fetchCurrentUserInfo = (freelanceAppService, dispatch) => {
  dispatch(currentUserInfoRequested());

  freelanceAppService
    .getCurrentUserInfo()
    .then((data) => dispatch(currentUserInfoLoaded(data)))
    .catch((err) => dispatch(currentUserInfoError(err)));
};

const logout = (dispatch) => {
  dispatch({
    type: "FETCH_LOGOUT_REQUEST",
  });
};

const fetchUserEditData = (freelanceAppService, dispatch, id) => {
  dispatch(userEditDataRequested());

  freelanceAppService
    .getUserDataForEdit(id)
    .then((data) => dispatch(userEditDataLoaded(data)))
    .catch((err) => dispatch(userEditDataError(err)));
};

const fetchUserNewPass = (freelanceAppService, dispatch, data) => {
  dispatch(userNewDataRequested());

  freelanceAppService
    .updateUserPass(data)
    .then((data) => dispatch(data))
    .catch((err) => dispatch(userNewDataError(err)));
};

const fetchUserNewAvatar = (freelanceAppService, dispatch, data) => {
  dispatch(userNewDataRequested());

  freelanceAppService
    .updateUserAvatar(data)
    .then((data) => dispatch(userNewDataLoaded(data)))
    .catch((err) => dispatch(userNewDataError(err)));
};

const fetchUserNewData = (freelanceAppService, dispatch, data) => {
  dispatch(userNewDataRequested());

  freelanceAppService
    .updateUserData(data)
    .then((data) => dispatch(userNewDataLoaded(data)))
    .catch((err) => dispatch(userNewDataError(err)));
};

const fetchDeclineUser = (freelanceAppService, dispatch, id) => {
  dispatch(verifyUserRequested());

  freelanceAppService
    .declineUser(id)
    .then((data) => dispatch(verifyUserLoaded(data)))
    .catch((err) => dispatch(verifyUserError(err)));
};

const fetchVerifyUser = (freelanceAppService, dispatch, id) => {
  dispatch(verifyUserRequested());

  freelanceAppService
    .verifyUser(id)
    .then((data) => dispatch(verifyUserLoaded(data)))
    .catch((err) => dispatch(verifyUserError(err)));
};

const fetchFreelancer = (freelanceAppService, dispatch, userId) => {
  dispatch(freelancerRequested());

  freelanceAppService
    .getFreelancer(userId)
    .then((data) => dispatch(freelancerLoaded(data)))
    .catch((err) => dispatch(freelancerError(err)));
};

const fetchUnverifiedFreelancers = (freelanceAppService, dispatch) => {
  dispatch(freelancersRequested());

  freelanceAppService
    .getUnverifiedFreelancers()
    .then((data) => dispatch(freelancersLoaded(data)))
    .catch((err) => dispatch(freelancersError(err)));
};

const fetchFreelancers = (freelanceAppService, dispatch) => {
  dispatch(freelancersRequested());

  freelanceAppService
    .getActiveFreelancers()
    .then((data) => dispatch(freelancersLoaded(data)))
    .catch((err) => dispatch(freelancersError(err)));
};

const fetchUnverifiedСlients = (freelanceAppService, dispatch) => {
  dispatch(clientsRequested());

  freelanceAppService
    .getUnverifiedClients()
    .then((data) => dispatch(clientsLoaded(data)))
    .catch((err) => dispatch(clientsError(err)));
};
const fetchСlients = (freelanceAppService, dispatch) => {
  dispatch(clientsRequested());

  freelanceAppService
    .getActiveClients()
    .then((data) => dispatch(clientsLoaded(data)))
    .catch((err) => dispatch(clientsError(err)));
};

const fetchCasesList = (freelanceAppService, dispatch, freelancerId) => {
  dispatch(casesListRequested());
  freelanceAppService
    .getUserCases(freelancerId)
    .then((data) => dispatch(casesListLoaded(data)))
    .catch((err) => dispatch(casesListError(err)));
};

const fetchCases = (freelanceAppService, dispatch) => {
  dispatch(casesRequested());
  freelanceAppService
    .getCases()
    .then((data) => dispatch(casesLoaded(data)))
    .catch((err) => dispatch(casesError(err)));
};

const onUnload = (dispatch) => {
  dispatch(projectUnload());
};

export {
  fetchCurrentUser,
  fetchСlients,
  fetchClient,
  fetchProject,
  fetchReviewsList,
  fetchFreelancer,
  fetchFreelancers,
  fetchUnverifiedFreelancers,
  fetchUnverifiedСlients,
  fetchCasesList,
  onUnload,
  fetchCases,
  fetchUserEditData,
  fetchUserNewAvatar,
  fetchUserNewPass,
  fetchUserNewData,
  fetchVerifyUser,
  fetchDeclineUser,
  fetchCurrentUserInfo,
  logout,
};
