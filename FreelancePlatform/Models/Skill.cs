﻿using System;
using System.Collections.Generic;

namespace FreelancePlatform.Models
{
    public partial class Skill
    {
        public Skill()
        {
            SkillFreelancer = new HashSet<SkillFreelancer>();
        }

        public int IdSkill { get; set; }
        public string Name { get; set; }

        public virtual ICollection<SkillFreelancer> SkillFreelancer { get; set; }
    }
}
