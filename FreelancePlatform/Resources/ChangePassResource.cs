﻿using System;
using Microsoft.AspNetCore.Http;

namespace FreelancePlatform.Resources
{
    public class ChangePassResource
    {
        public string Current { get; set; }
        public string New { get; set; }

    }
}
