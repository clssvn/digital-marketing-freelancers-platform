import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom'

import FreelanceAppService from './services/freelance-app-service';
import { FreelanceAppServiceProvider } from './components/freelance-app-service-context';
import App from './components/app';

import store from './store';

const freelanceAppService = new FreelanceAppService();

ReactDOM.render(
    <Provider store={store}>
            <FreelanceAppServiceProvider value={freelanceAppService}>
                <Router>
                    <App />
                </Router>
            </FreelanceAppServiceProvider>
    </Provider>,
    document.getElementById('root'));