﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace FreelancePlatform.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    id_user = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    email = table.Column<string>(maxLength: 50, nullable: false),
                    pass = table.Column<string>(maxLength: 30, nullable: false),
                    name = table.Column<string>(maxLength: 50, nullable: false),
                    surname = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("user_pk", x => x.id_user);
                });

            migrationBuilder.CreateTable(
                name: "avatar",
                columns: table => new
                {
                    id_avatar = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    avatarlink = table.Column<string>(maxLength: 300, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("avatar_pk", x => x.id_avatar);
                });

            migrationBuilder.CreateTable(
                name: "category",
                columns: table => new
                {
                    id_category = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("category_pk", x => x.id_category);
                });

            migrationBuilder.CreateTable(
                name: "country",
                columns: table => new
                {
                    id_country = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(maxLength: 70, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("country_pk", x => x.id_country);
                });

            migrationBuilder.CreateTable(
                name: "niche",
                columns: table => new
                {
                    id_niche = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(maxLength: 70, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("niche_pk", x => x.id_niche);
                });

            migrationBuilder.CreateTable(
                name: "skill",
                columns: table => new
                {
                    id_skill = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("skill_pk", x => x.id_skill);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    RoleId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "administrator",
                columns: table => new
                {
                    id_user = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("administrator_pk", x => x.id_user);
                    table.ForeignKey(
                        name: "administrator_user",
                        column: x => x.id_user,
                        principalTable: "AspNetUsers",
                        principalColumn: "id_user",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "id_user",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "id_user",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "id_user",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "id_user",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "contact",
                columns: table => new
                {
                    id_contact = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    type = table.Column<string>(maxLength: 20, nullable: false),
                    contact = table.Column<string>(maxLength: 50, nullable: false),
                    user_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("contact_pk", x => x.id_contact);
                    table.ForeignKey(
                        name: "contact_user",
                        column: x => x.user_id,
                        principalTable: "AspNetUsers",
                        principalColumn: "id_user",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "city",
                columns: table => new
                {
                    id_city = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(maxLength: 70, nullable: false),
                    country_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("city_pk", x => x.id_city);
                    table.ForeignKey(
                        name: "miasto_kraj",
                        column: x => x.country_id,
                        principalTable: "country",
                        principalColumn: "id_country",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "user_info",
                columns: table => new
                {
                    id_user = table.Column<int>(nullable: false),
                    is_verified = table.Column<bool>(nullable: false),
                    reg_date = table.Column<DateTime>(nullable: false),
                    info = table.Column<string>(nullable: false),
                    city_id = table.Column<int>(nullable: false),
                    avatar_id_avatar = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("user_info_pk", x => x.id_user);
                    table.ForeignKey(
                        name: "user_info_avatar",
                        column: x => x.avatar_id_avatar,
                        principalTable: "avatar",
                        principalColumn: "id_avatar",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "user_info_city",
                        column: x => x.city_id,
                        principalTable: "city",
                        principalColumn: "id_city",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "user_info_user",
                        column: x => x.id_user,
                        principalTable: "AspNetUsers",
                        principalColumn: "id_user",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "freelancer",
                columns: table => new
                {
                    id_user = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    category_id = table.Column<int>(nullable: false),
                    portfoliolink = table.Column<string>(maxLength: 300, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("freelancer_pk", x => x.id_user);
                    table.ForeignKey(
                        name: "freelancer_kategoria",
                        column: x => x.category_id,
                        principalTable: "category",
                        principalColumn: "id_category",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "freelancer_user_info",
                        column: x => x.id_user,
                        principalTable: "user_info",
                        principalColumn: "id_user",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "klient",
                columns: table => new
                {
                    id_user = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(maxLength: 100, nullable: true),
                    company_size = table.Column<int>(nullable: false),
                    niche_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("klient_pk", x => x.id_user);
                    table.ForeignKey(
                        name: "klient_user_info",
                        column: x => x.id_user,
                        principalTable: "user_info",
                        principalColumn: "id_user",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "klient_niche",
                        column: x => x.niche_id,
                        principalTable: "niche",
                        principalColumn: "id_niche",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "skill_freelancer",
                columns: table => new
                {
                    skill_id = table.Column<int>(nullable: false),
                    freelancer_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("skill_freelancer_pk", x => new { x.skill_id, x.freelancer_id });
                    table.ForeignKey(
                        name: "umietnosci_freelancer_freelancer",
                        column: x => x.freelancer_id,
                        principalTable: "freelancer",
                        principalColumn: "id_user",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "umietnosci_freelancer_umietnosci",
                        column: x => x.skill_id,
                        principalTable: "skill",
                        principalColumn: "id_skill",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "review",
                columns: table => new
                {
                    id_review = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    added_date = table.Column<DateTime>(nullable: false),
                    content = table.Column<string>(nullable: false),
                    client_id = table.Column<int>(nullable: false),
                    freelancer_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("review_pk", x => x.id_review);
                    table.ForeignKey(
                        name: "opinia_klient",
                        column: x => x.client_id,
                        principalTable: "klient",
                        principalColumn: "id_user",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "review_freelancer",
                        column: x => x.freelancer_id,
                        principalTable: "freelancer",
                        principalColumn: "id_user",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "project",
                columns: table => new
                {
                    id_project = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(maxLength: 50, nullable: false),
                    added_date = table.Column<DateTime>(nullable: false),
                    id_freelancer = table.Column<int>(nullable: false),
                    id_klient = table.Column<int>(nullable: false),
                    goals = table.Column<string>(nullable: false),
                    description = table.Column<string>(nullable: false),
                    result = table.Column<string>(nullable: false),
                    price = table.Column<decimal>(type: "money", nullable: false),
                    niche_id = table.Column<int>(nullable: false),
                    category_id = table.Column<int>(nullable: false),
                    review_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("project_pk", x => x.id_project);
                    table.ForeignKey(
                        name: "projekt_kategoria",
                        column: x => x.category_id,
                        principalTable: "category",
                        principalColumn: "id_category",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "projekt_freelancer",
                        column: x => x.id_freelancer,
                        principalTable: "freelancer",
                        principalColumn: "id_user",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "projekt_klient",
                        column: x => x.id_klient,
                        principalTable: "klient",
                        principalColumn: "id_user",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "projekt_nisza",
                        column: x => x.niche_id,
                        principalTable: "niche",
                        principalColumn: "id_niche",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "projekt_opinia",
                        column: x => x.review_id,
                        principalTable: "review",
                        principalColumn: "id_review",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "image_file",
                columns: table => new
                {
                    id_file = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    project_id = table.Column<int>(nullable: false),
                    filelink = table.Column<string>(maxLength: 300, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("image_file_pk", x => x.id_file);
                    table.ForeignKey(
                        name: "plik_graficzny_projekt",
                        column: x => x.project_id,
                        principalTable: "project",
                        principalColumn: "id_project",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_city_country_id",
                table: "city",
                column: "country_id");

            migrationBuilder.CreateIndex(
                name: "IX_contact_user_id",
                table: "contact",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_freelancer_category_id",
                table: "freelancer",
                column: "category_id");

            migrationBuilder.CreateIndex(
                name: "IX_image_file_project_id",
                table: "image_file",
                column: "project_id");

            migrationBuilder.CreateIndex(
                name: "IX_klient_niche_id",
                table: "klient",
                column: "niche_id");

            migrationBuilder.CreateIndex(
                name: "IX_project_category_id",
                table: "project",
                column: "category_id");

            migrationBuilder.CreateIndex(
                name: "IX_project_id_freelancer",
                table: "project",
                column: "id_freelancer");

            migrationBuilder.CreateIndex(
                name: "IX_project_id_klient",
                table: "project",
                column: "id_klient");

            migrationBuilder.CreateIndex(
                name: "IX_project_niche_id",
                table: "project",
                column: "niche_id");

            migrationBuilder.CreateIndex(
                name: "IX_project_review_id",
                table: "project",
                column: "review_id");

            migrationBuilder.CreateIndex(
                name: "IX_review_client_id",
                table: "review",
                column: "client_id");

            migrationBuilder.CreateIndex(
                name: "IX_review_freelancer_id",
                table: "review",
                column: "freelancer_id");

            migrationBuilder.CreateIndex(
                name: "IX_skill_freelancer_freelancer_id",
                table: "skill_freelancer",
                column: "freelancer_id");

            migrationBuilder.CreateIndex(
                name: "IX_user_info_avatar_id_avatar",
                table: "user_info",
                column: "avatar_id_avatar");

            migrationBuilder.CreateIndex(
                name: "IX_user_info_city_id",
                table: "user_info",
                column: "city_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "administrator");

            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "contact");

            migrationBuilder.DropTable(
                name: "image_file");

            migrationBuilder.DropTable(
                name: "skill_freelancer");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "project");

            migrationBuilder.DropTable(
                name: "skill");

            migrationBuilder.DropTable(
                name: "review");

            migrationBuilder.DropTable(
                name: "klient");

            migrationBuilder.DropTable(
                name: "freelancer");

            migrationBuilder.DropTable(
                name: "niche");

            migrationBuilder.DropTable(
                name: "category");

            migrationBuilder.DropTable(
                name: "user_info");

            migrationBuilder.DropTable(
                name: "avatar");

            migrationBuilder.DropTable(
                name: "city");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "country");
        }
    }
}
