﻿using System;
using System.Collections.Generic;
using FreelancePlatform.Models;
using Microsoft.AspNetCore.Http;

namespace FreelancePlatform.Resources
{
    public class FreelancerSignUpResource
    {
        public UserSignUpResource UserSignUpResource { get; set; }
        public FreelancerSignUpDataResource FreelancerSignUpDataResource { get; set; }
        public UserInfoSignUpResource UserInfoSignUpResource { get; set; }

        public ICollection<SkillFreelancerResource> SkillFreelancer { get; set; }
    }

    public class FreelancerSignUpDataResource
    {
        public int CategoryId { get; set; }
        public IFormFile Portfolio { get; set; }
    }

}
