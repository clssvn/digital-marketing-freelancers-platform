import React, { Component, useState } from "react";
import { Container, Row, Col, Button, Modal, Image } from "react-bootstrap";
import "./admin-dashboard.scss";
import {
  fetchUnverifiedFreelancers,
  fetchUnverifiedСlients,
  fetchVerifyUser,
  fetchDeclineUser,
} from "../../actions";
import { compose } from "redux";
import { connect } from "react-redux";
import { withFreelanceAppService } from "../hoc";
import ClientCard from "../client-card";
import FreelancerCard from "../freelancer-card";
import Loading from "../loading/loading";

const SmallUserCard = ({ avatar, name, surname, city }) => {
  return (
    <Container className="user-card sm">
      <Row>
        <Col sm={4} className="image-col">
          <Image src={avatar} className="avatar" alt="avatar" />
        </Col>
        <Col sm={8} className="data-col sm-data">
          <Row className="name">{name + " " + surname}</Row>
          <Row className="city-regdate">{city}</Row>
        </Col>
      </Row>
    </Container>
  );
};

const VerificationModal = ({ body, show, setShow }) => {
  return (
    <Modal show={show} fullscreen={true} onHide={() => setShow(false)}>
      <Modal.Header closeButton />
      {body}
    </Modal>
  );
};

const UserForVerification = ({
  type,
  user,
  handleAcceptClick,
  handleDeclineClick,
}) => {
  const Resume = () => {
    if (type === "freelancers") {
      return (
        <Row className="resume">
          <h2>Resume</h2>
          <Button
            className="resume-btn"
            href={user.portfoliolink}
            target="_blank"
          >
            Open
          </Button>
        </Row>
      );
    }
    return null;
  };

  const UserCard = () => {
    switch (type) {
      case "freelancers": {
        var freelancer = user;
        return <FreelancerCard freelancer={{ freelancer }} />;
      }
      case "clients": {
        var client = user;
        return <ClientCard client={{ client }} />;
      }
      default:
        return null;
    }
  };

  return (
    <Container className="verifPage">
      <UserCard />
      <h2>About me</h2>
      <div className="u-info">{user.info}</div>
      <Resume />
      <h2>Contact</h2>
      <Row className="contact-row">
        <Image
          className="contact-logo"
          src={`/images/${user.contact.type}.png`}
        />
        <div className="contact-content">{user.contact.contact1}</div>
      </Row>
      <Row className="button-row">
        <Button className="decline" onClick={() => handleDeclineClick(user.id)}>
          Decline
        </Button>
        <Button className="accept" onClick={() => handleAcceptClick(user.id)}>
          Accept
        </Button>
      </Row>
    </Container>
  );
};

const Bar = ({
  type,
  users,
  loading,
  error,
  fetchVerifyUser,
  fetchDeclineUser,
  adminData,
  updateUsers,
}) => {
  const [show, setShow] = useState(false);
  const [user, setUser] = useState();
  const [result, setResult] = useState();

  const handleClick = (u) => {
    setUser(u);
    setShow(true);
  };

  const handleAcceptClick = (id) => {
    fetchVerifyUser(id);
    setResult(false);
  };

  const handleDeclineClick = (id) => {
    fetchDeclineUser(id);
    setResult(false);
  };
  if (loading) {
    return <Loading />;
  }

  if (!adminData.loading && show === true && !result) {
    setShow(false);
    setUser();
    setResult(true);
    updateUsers();
  }

  if (show) {
    return (
      <VerificationModal
        body={
          <UserForVerification
            type={type}
            user={user}
            handleAcceptClick={handleAcceptClick}
            handleDeclineClick={handleDeclineClick}
          />
        }
        show={show}
        setShow={setShow}
      />
    );
  }

  switch (type) {
    case "clients": {
      return Array.from(users).map((client) => {
        return (
          <li key={client.id} onClick={() => handleClick(client)}>
            <SmallUserCard
              avatar={client.avatar}
              name={client.name}
              surname={client.surname}
              city={client.city}
            />
          </li>
        );
      });
    }
    case "freelancers": {
      return Array.from(users).map((freelancer) => {
        return (
          <li key={freelancer.id} onClick={() => handleClick(freelancer)}>
            <SmallUserCard
              avatar={freelancer.avatar}
              name={freelancer.name}
              surname={freelancer.surname}
              city={freelancer.city}
            />
          </li>
        );
      });
    }
    default:
      return null;
  }
};

class AdminDashboard extends Component {
  componentDidMount() {
    this.props.fetchUnverifiedFreelancers();
    this.props.fetchUnverifiedСlients();
  }

  updateFreelancers = () => {
    this.props.fetchUnverifiedFreelancers();
  };

  updateСlients = () => {
    this.props.fetchUnverifiedСlients();
  };

  render() {
    const {
      adminData,
      clients,
      freelancers,
      fetchVerifyUser,
      fetchDeclineUser,
    } = this.props;

    return (
      <Container className="admin-dashboard">
        <Row>
          <Col className="bar-col">
            <h2>FREELANCERS</h2>
            <Bar
              type="freelancers"
              users={freelancers.freelancers}
              loading={freelancers.loading}
              error={freelancers.error}
              fetchVerifyUser={fetchVerifyUser}
              fetchDeclineUser={fetchDeclineUser}
              adminData={adminData}
              updateUsers={this.updateFreelancers}
            />
          </Col>
          <Col className="bar-col">
            <h2>CLIENTS</h2>
            <Bar
              type="clients"
              users={clients.clients}
              loading={clients.loading}
              error={clients.error}
              fetchVerifyUser={fetchVerifyUser}
              fetchDeclineUser={fetchDeclineUser}
              adminData={adminData}
              updateUsers={this.updateСlients}
            />
          </Col>
        </Row>
      </Container>
    );
  }
}

const mapDispatchToProps = (dispatch, { freelanceAppService }) => {
  return {
    fetchDeclineUser: (id) =>
      fetchDeclineUser(freelanceAppService, dispatch, id),
    fetchVerifyUser: (id) => fetchVerifyUser(freelanceAppService, dispatch, id),
    fetchUnverifiedFreelancers: () =>
      fetchUnverifiedFreelancers(freelanceAppService, dispatch),
    fetchUnverifiedСlients: () =>
      fetchUnverifiedСlients(freelanceAppService, dispatch),
  };
};

const mapStateToProps = ({ adminData, clients, freelancers }) => {
  return { adminData, clients, freelancers };
};

export default compose(
  withFreelanceAppService(),
  connect(mapStateToProps, mapDispatchToProps)
)(AdminDashboard);
