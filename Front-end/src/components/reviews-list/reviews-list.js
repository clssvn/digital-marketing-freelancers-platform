import React, { useState, Component } from "react";
import Error from "../error/error";
import Loading from "../loading/loading";
import { ReviewCard } from "../review-card";

const ShowMoreButton = ({ onClick }) => {
  return (
    <button className="btn btn-outline-primary btn-sm" onClick={onClick}>
      Read more
    </button>
  );
};

const ShowMoreListData = ({ reviewsList }) => {
  const [showedReviews, setShowedReviews] = useState(reviewsList.slice(0, 2));
  const [isClicked, setIsClicked] = useState(false);

  if (isClicked) {
    return <ReviewsListData reviewsList={showedReviews} />;
  }

  return (
    <>
      <ReviewsListData reviewsList={showedReviews} />
      <ShowMoreButton
        onClick={() => {
          setIsClicked(true);
          setShowedReviews(reviewsList);
        }}
      />
    </>
  );
};

const ReviewsListData = ({ reviewsList }) => {
  return reviewsList.map((review) => {
    return (
      <li key={review.id}>
        <ReviewCard>{review}</ReviewCard>
      </li>
    );
  });
};

class ReviewsList extends Component {
  render() {
    const { review, loading, error } = this.props.reviews;

    if (loading) {
      return <Loading />;
    }

    if(error){
      return <Error />;
  }

    if (review.length === 0) {
      return <h4 className="no-cases">No reviews</h4>;
    }

    if (review.length > 2) {
      return <ShowMoreListData reviewsList={review} />;
    }

    if (review.length <= 2) {
      return <ReviewsListData reviewsList={review} />;
    }
  }
}


export default ReviewsList;
