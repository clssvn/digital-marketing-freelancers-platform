import "./home.css";

import React, { Component } from "react";
import { UserRoutingHeader } from "../../header";

import FreelancerProfile from "../freelancer-profile/freelancer-profile";
import ClientProfile from "../client-profile/client-profile";

import Login from "../login/login";

import {
  fetchCurrentUserInfo
} from "../../../actions";
import { compose } from "redux";
import { connect } from "react-redux";
import { withFreelanceAppService } from "../../hoc";
import AdminDashboard from "../../admin-dashboard/admin-dashboard";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const UserProfile = ({ id, role }) => {
  switch (role) {
    case "freelancer": {
      return <FreelancerProfile id={id} />;
    }
    case "client": {
      return <ClientProfile id={id} />;
    }
    case "admin": {
      return <AdminDashboard />;
    }
    default:
      return null;
  }
};

class Home extends Component {
  isLogged = false;

  componentDidMount() {
    if (localStorage.getItem("token") != null) {
      this.props.fetchCurrentUserInfo();
      this.isLogged = true;
    }
  }

  render() {
    if (this.props.currentUser.error != null) {
      toast.error("Something went wrong. Please try again");
    }

    if (localStorage.getItem("token") == null) {
      return <Login />;
    }

    if (!this.isLogged) {
      this.props.fetchCurrentUserInfo();
      this.isLogged = true;
    }
    

    return (
      <div className="container-flud fl-page">
        <UserRoutingHeader
          role={this.props.currentUser.currentUser.role}
          home={true}
        />
        <UserProfile {...this.props.currentUser.currentUser} />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch, { freelanceAppService }) => {
  return {
    fetchCurrentUserInfo: () =>
      fetchCurrentUserInfo(freelanceAppService, dispatch),
  };
};

const mapStateToProps = ({ currentUser }) => {
  return { currentUser };
};

export default compose(
  withFreelanceAppService(),
  connect(mapStateToProps, mapDispatchToProps)
)(Home);
