import "./freelancers-page.scss";

import React, { Component } from "react";
import { UserRoutingHeader } from "../../header";
import { Image, Row } from "react-bootstrap";

import FreelancerCard from "../../freelancer-card";
import CaseList from "../../case-list";
import ReviewsList from "../../reviews-list";

import { fetchFreelancer, fetchCasesList, fetchReviewsList, fetchCurrentUserInfo } from "../../../actions";
import { compose } from "redux";
import { connect } from "react-redux";
import { withFreelanceAppService } from "../../hoc";
import { AddReview } from "..";
import Loading from "../../loading/loading";
import Error from "../../error/error";

const ReviewOpportunity = ({caseList, currentUserId}) => {
  if (!caseList.loading){
    var casesForReviewByCurrUser = [];
    caseList.cases.forEach(c => {
      if(c.klient.id === currentUserId && c.reviewId == null){
        casesForReviewByCurrUser = [...casesForReviewByCurrUser, c]
      }
    })
    

    if(casesForReviewByCurrUser.length > 0){
      return(
        <AddReview projects={casesForReviewByCurrUser}/>
      )
    }
  }
  return null;
}

const Reviews = ({reviews}) => {
  return (
    <>
      <div className="header">REVIEWS</div>
      <ReviewsList reviews={reviews} />
    </>
  );
};

const Cases = ({cases}) => {
  return (
    <>
      <div className="justify-content-center projects card-deck">
        <CaseList cases={cases} />
      </div>
    </>
  );
};

class FreelancerPage extends Component {
  userId = this.props.match.params.id;

  componentDidMount() {
    this.props.fetchCasesList(this.userId);
    this.props.fetchReviewsList(this.userId);
    this.props.fetchFreelancer(this.userId);
    this.props.fetchCurrentUserInfo();
  }
  render() {
      const { freelancer, caseList, review, currentUser } = this.props;
      if (freelancer.loading) {
        return <Loading />;
      }

      if(freelancer.error){
        return <Error />;
    }
      return (
      <div className="container-flud fl-page justify-content-center">
        <UserRoutingHeader />
        
        <FreelancerCard freelancer={freelancer} />
        <h4 className="header">About me</h4>
        <div className="no-cases">{freelancer.freelancer.info}</div>

        <hr style={{color: "#acacac"}}/>
        <div style={{ textAlign: "center" }}> <h4 className="header">PORTFOLIO</h4> </div>

        <Cases cases={caseList} />
        <hr style={{color: "#acacac"}}/>

        <Reviews reviews={review} />
        <ReviewOpportunity caseList={caseList} currentUserId={currentUser.currentUser.id}/>
        <hr style={{color: "#acacac"}}/>

        <div className="header">Contact with {freelancer.freelancer.name}</div>
        <Row className="contact-row">
          <Image className="contact-logo" src={`/images/${freelancer.freelancer.contact.type}.png`} />
          <div className="contact-content" >{freelancer.freelancer.contact.contact1}</div>
        </Row>
      </div>
    );
  }
}


const mapDispatchToProps = (dispatch, { freelanceAppService }) => {
    return {
        fetchCurrentUserInfo: () => fetchCurrentUserInfo(freelanceAppService, dispatch),
        fetchCasesList: (freelancerId) => fetchCasesList(freelanceAppService, dispatch, freelancerId),
        fetchReviewsList: (freelancerId) => fetchReviewsList(freelanceAppService, dispatch, freelancerId),
        fetchFreelancer: (freelancerId) => fetchFreelancer(freelanceAppService, dispatch, freelancerId)
    };
  };
  
  const mapStateToProps = ({ freelancer, caseList, review, currentUser }) => {
    return { freelancer, caseList, review, currentUser };
  };
  
  export default compose(
    withFreelanceAppService(),
    connect(mapStateToProps, mapDispatchToProps)
  )(FreelancerPage);