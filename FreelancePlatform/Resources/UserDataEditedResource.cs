﻿using System;
using System.Collections.Generic;

namespace FreelancePlatform.Resources
{
    public class UserDataEditedResource
    {
        public ContactResource? Contact { get; set; }
        public string? Info { get; set; }
        public ICollection<SkillFreelancerResource>? SkillFreelancerResource { get; set; }

    }
}
