﻿using System;
using System.Collections.Generic;

namespace FreelancePlatform.Resources
{
    public class ReviewResource
    {
        public int IdReview { get; set; }
        public DateTime AddedDate { get; set; }
        public KlientReviewResource Klient { get; set; }
        public string Content { get; set; }
        public ProjectReviewResource Project { get; set; }

    }

    public class ProjectReviewResource
    {
        public int IdProject { get; set; }
        public string Name { get; set; }
    }

    public class KlientReviewResource
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string MemberFor { get; set; }
        public string Avatar { get; set; }
        public string CompanyName { get; set; }
    }
}
