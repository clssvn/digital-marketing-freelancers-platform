import React from 'react'
import { Navbar } from 'react-bootstrap';
import './header.scss'

export const MainHeader = () => {
    return(
        <Navbar className="main-header" expand="lg">
        <Navbar.Brand href="/">#TopM</Navbar.Brand>
        </Navbar>
    )
}