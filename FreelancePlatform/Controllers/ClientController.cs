﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FreelancePlatform.Models;
using FreelancePlatform.Resources;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FreelancePlatform.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class ClientController : ControllerBase
    {
        private topmContext _context;
        private readonly IMapper _mapper;

        public ClientController(topmContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        //LISTA Klientow - do weryfikacji
        [Authorize(Roles = "admin")]
        [HttpGet("unverified")]
        public async Task<ActionResult<IEnumerable<KlientResource>>> GetUnVerifiedClients()
        {
            var klients = await _context.Klient.Where(f => f.IdNavigation.IsVerified == false).ToListAsync();

            if (klients == null)
            {
                return NoContent();
            }

            var klientsResources = _mapper.Map<IEnumerable<Klient>, IEnumerable<KlientResource>>(klients).ToList();

            return Ok(klientsResources);
        }

        //LISTA Klientow - aktywne
        [Authorize]
        [HttpGet("active")]
        public async Task<ActionResult<IEnumerable<KlientResource>>> GetActiveClients()
        {
            var klients = await _context.Klient.Where(f => f.IdNavigation.IsVerified == true).ToListAsync();

            if (klients == null)
            {
                return NoContent();
            }

            var klientsResources = _mapper.Map<IEnumerable<Klient>, IEnumerable<KlientResource>>(klients).ToList();

            return Ok(klientsResources);
        }

        //Klient
        [Authorize]
        [HttpGet("{id:int}")]
        public async Task<ActionResult<KlientResource>> GetClient(int id)
        {
            var klient = await _context.Klient.FirstOrDefaultAsync(e => e.Id == id);

            if (klient == null)
            {
                return NoContent();
            }

            var klientResource = _mapper.Map<Klient, KlientResource>(klient);

            return Ok(klientResource);

        }

        [Authorize]
        [HttpGet("q={name}")]
        public async Task<ActionResult<KlientQueryResource>> SearchClient(string name)
        {
            var klients = await _context.Klient
                .Where(e => e.IdNavigation.IdNavigation.Name.ToLower().Contains(name.ToLower()) ||
                e.IdNavigation.IdNavigation.Surname.ToLower().Contains(name.ToLower())).ToListAsync();

            Console.WriteLine(klients);
            var klientsResources = _mapper.Map<IEnumerable<Klient>, IEnumerable<KlientQueryResource>>(klients).ToList();

            return Ok(klientsResources);

        }
    }
}
