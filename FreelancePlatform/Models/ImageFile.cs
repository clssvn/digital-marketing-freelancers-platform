﻿using System;
using System.Collections.Generic;

namespace FreelancePlatform.Models
{
    public partial class ImageFile
    {
        public int IdFile { get; set; }
        public int ProjectId { get; set; }
        public string Filelink { get; set; }

        public virtual Project Project { get; set; }

        public ImageFile(string filelink)
        {
            Filelink = filelink;
        }
    }
}
