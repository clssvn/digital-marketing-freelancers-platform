﻿using System;
using System.Collections.Generic;

namespace FreelancePlatform.Models
{
    public partial class SkillFreelancer
    {
        public int SkillId { get; set; }
        public int FreelancerId { get; set; }

        public virtual Freelancer Freelancer { get; set; }
        public virtual Skill Skill { get; set; }
    }
}
