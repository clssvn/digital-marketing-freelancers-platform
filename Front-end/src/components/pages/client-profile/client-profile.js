import "./client-profile.css";

import React, { Component } from "react";

import ClientCard from "../../client-card";
import CaseList from "../../case-list";

import { fetchClient, fetchCasesList } from "../../../actions";
import { compose } from "redux";
import { connect } from "react-redux";
import { withFreelanceAppService } from "../../hoc";
import { Image, Row } from "react-bootstrap";
import Loading from "../../loading/loading";
import Error from "../../error/error";

const Cases = ({ cases }) => {
  return (
    <>
      <div className="projects card-deck">
        <CaseList cases={cases} />
      </div>
    </>
  );
};

class ClientProfile extends Component {
  componentDidMount() {
    this.props.fetchCasesList(this.props.id);
    this.props.fetchClient(this.props.id);
  }

  render() {
    const { client, caseList } = this.props;

    if (client.loading || caseList.loading) {
      return <Loading />;
    }
    if(client.error || caseList.error){
      console.log(client)
      return <Error />;
  }

    return (
      <div className="container-flud fl-page">
        <ClientCard client={client} />
        <h4 className="header">About me</h4>
        <div className="no-cases">{client.client.info}</div>
        <hr style={{ color: "#acacac" }} />
        <div className="header">PROJECTS HISTORY</div>
        <Cases cases={caseList} />
        <div className="header">Your contact</div>
        <Row className="contact-row">
          <Image
            className="contact-logo"
            src={`/images/${client.client.contact.type}.png`}
          />
          <div>{client.client.contact.contact1}</div>
        </Row>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch, { freelanceAppService }) => {
  return {
    fetchCasesList: (clientId) =>
      fetchCasesList(freelanceAppService, dispatch, clientId),
    fetchClient: (clientId) =>
      fetchClient(freelanceAppService, dispatch, clientId),
  };
};

const mapStateToProps = ({ client, caseList }) => {
  return { client, caseList };
};

export default compose(
  withFreelanceAppService(),
  connect(mapStateToProps, mapDispatchToProps)
)(ClientProfile);
