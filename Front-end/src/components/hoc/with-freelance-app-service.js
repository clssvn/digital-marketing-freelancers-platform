import React from 'react';
import { FreelanceAppServiceConsumer } from '../freelance-app-service-context';

const withFreelanceAppService = () => (Wrapped) => {
    return (props) => {
        return (
            <FreelanceAppServiceConsumer>
            {
                (freelanceAppService) => {
                    return (<Wrapped {...props}
                        freelanceAppService={freelanceAppService} />);
                }
            }
            </FreelanceAppServiceConsumer>
        )
    }
}

export default withFreelanceAppService;