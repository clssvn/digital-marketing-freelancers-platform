import React, { Component } from "react";
import "./freelancer-card.scss";
import { Row, Col, Image } from "react-bootstrap";
import Loading from "../loading/loading";
import Error from "../error/error";

class FreelancerCard extends Component {
  render() {
    const { freelancer, loading, error } = this.props.freelancer;

    if (loading) {
      return <Loading />;
    }

    if(error){
      return <Error />;
  }

    const { avatar, name, memberFor, category, surname, city, skill = [] } = freelancer;
    return (
      <div className="user-card">
        <Row>
          <Col sm={4} className="image-col">
            <Image src={avatar} className="avatar" alt="avatar" />
          </Col>
          <Col sm={8} className="data-col">
            <Row className="name">{name + " " + surname}</Row>
            <Row className="city-regdate">
              {city} | Member for {memberFor}
            </Row>
            <Row className="category-freelancercard">{category.name}</Row>
            <Row className="skills">
              <ul className="user-skills">
                {skill.map((q) => {
                  return <li className="d-inline">{q.name}</li>;
                })}
              </ul>
            </Row>
          </Col>
        </Row>
      </div>
    );
  }
}

export default FreelancerCard;
