import React from "react";
import { MainHeader } from "../../header";
import './confirm-page.css';

const ConfirmPage = () => {
  return (
    <>
      <MainHeader />
      <div className="confirm-page-1">Thank you for your registratoin</div>
      <div className="confirm-page-2">
        Please, wait for verification. Our admin will contact you after
        verification and then you'll be able to sign in
      </div>
    </>
  );
};

export default ConfirmPage;
