import React, { useState } from 'react'
import { Button, Modal } from 'react-bootstrap';


const Error = (error) => {
    const [show, setShow] = useState(true);

    const onHide = () => {setShow(false)};
    return (
      <Modal
        show={show}
        onHide={onHide}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Error
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            Something went wrong.
            Please, try again
          </p>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={onHide}>Close</Button>
        </Modal.Footer>
      </Modal>
    );
  }

  export default Error;