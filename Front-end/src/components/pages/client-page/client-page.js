import React, { Component } from "react";
import { UserRoutingHeader } from "../../header";
import { Image, Row } from "react-bootstrap";

import ClientCard from "../../client-card";
import CaseList from "../../case-list";
import "./client-page.scss";
import { fetchClient, fetchCasesList } from "../../../actions";
import { compose } from "redux";
import { connect } from "react-redux";
import { withFreelanceAppService } from "../../hoc";
import Loading from "../../loading/loading";
import Error from "../../error/error";

const Cases = ({ cases }) => {
  return (
    <>
      <div className="projects card-deck">
        <CaseList cases={cases} />
      </div>
    </>
  );
};

class ClientPage extends Component {
  userId = this.props.match.params.id;

  componentDidMount() {
    this.props.fetchCasesList(this.userId);
    this.props.fetchClient(this.userId);
  }

  render() {
    const { client, caseList } = this.props;
    if (client.loading) {
      return <Loading />;
    }
    if(client.error){
      return <Error />;
  }
    return (
      <div className="container-flud fl-page">
        <UserRoutingHeader />
        <ClientCard client={client} />
        <h4 className="header">About me</h4>
        <div className="no-cases">{client.client.info}</div>
        <hr style={{ color: "#acacac" }} />
        <div style={{ textAlign: "center" }}>
          <h4 className="header">PROJECTS HISTORY</h4>
        </div>
        <Cases cases={caseList} />
        <div className="header">Contact with {client.client.name}</div>
        <Row className="contact-row">
          <Image className="contact-logo" src={`/images/${client.client.contact.type}.png`} />
          <div>{client.client.contact.contact1}</div>
        </Row>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch, { freelanceAppService }) => {
  return {
    fetchCasesList: (clientId) =>
      fetchCasesList(freelanceAppService, dispatch, clientId),
    fetchClient: (clientId) =>
      fetchClient(freelanceAppService, dispatch, clientId),
  };
};

const mapStateToProps = ({ client, caseList }) => {
  return { client, caseList };
};

export default compose(
  withFreelanceAppService(),
  connect(mapStateToProps, mapDispatchToProps)
)(ClientPage);
