import React from "react";
import { Component } from "react";
import AsyncSelect from "react-select/async";
import makeAnimated from "react-select/animated";
import { withRouter } from "react-router-dom";

import {
  Container,
  Row,
  Col,
  Button,
  Form,
  InputGroup,
  FormControl,
} from "react-bootstrap";
import "./add-case.scss";
import Error from "../../error/error";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { MainHeader } from "../../header";
import configData from "../../../config.json";

const animatedComponents = makeAnimated();
const token = localStorage.getItem("token");
const _apiBase = configData.SERVER_URL;

const postResource = async (data) => {
  const res = await fetch(`${_apiBase}Cases`, {
    method: "POST",
    headers: {
      Authorization: `Bearer ${token}`,
    },
    body: data,
  });

  return res;
};

class AddCase extends Component {
  state = {
    btnDisabled: false,
    validated: false,
    query: "",
    name: "",
    idKlient: "",
    goals: "",
    description: "",
    result: "",
    price: null,
    categoryId: "",
    nicheId: "",
    imageFile: [],
  };

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleSubmit = async (event, notify, notifyOk) => {
    this.setState({ btnDisabled: true });
    const form = event.currentTarget;
    if (
      form.checkValidity() === false ||
      this.state.idKlient === "" ||
      this.state.categoryId === "" ||
      this.state.nicheId === ""
    ) {
      event.preventDefault();
      event.stopPropagation();
      notify("Please, fill all required fields");
      this.setState({ btnDisabled: false });
      return;
    } else {
      event.preventDefault();

      const formData = new FormData();
      formData.append("name", this.state.name);
      formData.append("idKlient", this.state.idKlient);
      formData.append("goals", this.state.goals);
      formData.append("description", this.state.description);
      formData.append("result", this.state.result);
      formData.append("price", this.state.price);
      formData.append("nicheId", 0);
      formData.append("categoryId", 0);

      const files = this.state.imageFile;

      for (let i = 0; i < files.length; i++) {
        formData.append("imageFileUpload", files[i]);
      }

      var res = await postResource(formData);
      if (!res.ok) {
        notify("Something went wrong. Please, tre again.");
      }

      if (res.ok) {
        notifyOk("Project added succesfuly");
        this.props.history.push("/");
      }
    }
    this.setState({ btnDisabled: false });
  };

  uploadFiles = (event) => {
    this.setState({
      imageFile: event.target.files,
    });
  };

  loadOptionsClient = async () => {
    if (this.state.query.trim() === "") return;

    const res = await fetch(
      `${_apiBase}Client/q=${this.state.query}`, 
       {
        method: "GET",
        headers: {
            'Authorization': `Bearer ${token}`
        }
    }
    );

    if (!res.ok) {
      return <Error />;
    }
    return await res.json().then((result) => {
      return result;
    });
  };

  loadOptionsCategory = async () => {
    const res = await fetch(`${_apiBase}Category`);

    if (!res.ok) {
      return <Error />;
    }

    return await res.json().then((result) => {
      return result;
    });
  };

  loadOptionsNiche = async () => {
    const res = await fetch(`${_apiBase}Niche`);

    if (!res.ok) {
      return <Error />;
    }
    return await res.json().then((result) => {
      return result;
    });
  };

  notify = (msg) => toast.error(msg);
  notifyOk = (msg) => toast.success(msg);
  render() {
    return (
      <>
        <MainHeader />
        <Container>
          <Row className="project-add-title">Add project to portfolio</Row>
          <hr style={{ color: "#acacac" }} />
          <Form
            noValidate
            validated={this.state.validated}
            onSubmit={(event) =>
              this.handleSubmit(
                event,
                (msg) => this.notify(msg),
                (msg) => this.notifyOk(msg)
              )
            }
          >
            <Row className="justify-content-md-center">
              <Col sm={4}>
                <Form.Label for="client">Client</Form.Label>
                <AsyncSelect
                  className="asyncselect"
                  cacheOptions
                  placeholder="Type to search..."
                  components={animatedComponents}
                  loadOptions={this.loadOptionsClient}
                  getOptionLabel={(e) => e.fullName}
                  getOptionValue={(e) => e.id}
                  onInputChange={(value) => this.setState({ query: value })}
                  onChange={(value) =>
                    this.setState({ query: "", idKlient: value.id })
                  }
                />

                <Form.Label for="category">Category</Form.Label>
                <AsyncSelect
                  className="asyncselect"
                  cacheOptions
                  components={animatedComponents}
                  defaultOptions
                  loadOptions={this.loadOptionsCategory}
                  getOptionLabel={(e) => e.name}
                  getOptionValue={(e) => e.idCategory}
                  onInputChange={(value) => this.setState({ query: value })}
                  onChange={(value) =>
                    this.setState({ query: "", categoryId: value.idCategory })
                  }
                />

                <Form.Label for="niche">Niche</Form.Label>
                <AsyncSelect
                  className="asyncselect"
                  cacheOptions
                  components={animatedComponents}
                  defaultOptions
                  loadOptions={this.loadOptionsNiche}
                  getOptionLabel={(e) => e.name}
                  getOptionValue={(e) => e.idNiche}
                  onInputChange={(value) => this.setState({ query: value })}
                  onChange={(value) =>
                    this.setState({ query: "", nicheId: value.idNiche })
                  }
                />

                <Form.Label for="price">Price</Form.Label>
                <InputGroup className="mb-3">
                  <FormControl
                    required
                    type="text"
                    name="price"
                    id="price"
                    onChange={this.handleChange}
                  />
                  <InputGroup.Text>PLN</InputGroup.Text>
                </InputGroup>
                <Form.Label for="file-upload">Add images</Form.Label>
                <Form.Control
                  required
                  type="file"
                  className="file-upload"
                  accept="image/*"
                  onChange={this.uploadFiles}
                  multiple
                />
              </Col>

              <Col sm={8} className="">
                <Form.Label for="name">Name</Form.Label>
                <Form.Control
                  required
                  type="text"
                  class="form-control"
                  name="name"
                  id="name"
                  onChange={this.handleChange}
                />

                <Form.Label for="goals">Goals</Form.Label>
                <Form.Control
                  required
                  as="textarea"
                  type="text"
                  class="form-control"
                  name="goals"
                  id="goals"
                  onChange={this.handleChange}
                />

                <Form.Label for="description">Description</Form.Label>
                <Form.Control
                  required
                  as="textarea"
                  type="text"
                  class="form-control"
                  name="description"
                  id="description"
                  onChange={this.handleChange}
                />

                <Form.Label for="result">Result</Form.Label>
                <Form.Control
                  required
                  as="textarea"
                  type="text"
                  class="form-control"
                  name="result"
                  id="result"
                  onChange={this.handleChange}
                />

                <Button
                  type="submit"
                  disabled={this.state.btnDisabled}
                  className="add-case-submit"
                >
                  Submit
                </Button>
              </Col>
            </Row>
          </Form>
        </Container>
      </>
    );
  }
}

export default withRouter(AddCase);
