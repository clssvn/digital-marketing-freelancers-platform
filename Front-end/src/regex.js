export const validEmail = new RegExp(
    '^[a-zA-Z0-9]+@(?:[a-zA-Z0-9]+\.)+[A-Za-z]+$'
 );
export const validPassword = new RegExp('^(?=.*?[A-Za-z])(?=.*[A-Z])(?=.*?[0-9])(?=.*[@$!%*?&]).{8,30}$');