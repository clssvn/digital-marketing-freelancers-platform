import React from "react";
import { Col, Container, Image, Row } from "react-bootstrap";
import "./review-card.scss";

const ReviewCard = (props) => {
  const {
    klient: { id, name, surname, avatar, companyName, memberFor },
    project: { idProject, name: projectName },
    content,
  } = props.children;

  return (
    <Row className="review-card">
      <Col sm={4} className="review-avatar">
        <Image className="review-avatar-img" src={avatar} />
      </Col>

      <Col sm={8} className="review-data">
        <div className="review-name">{name} {surname}</div>
        <div className="review-company">
          {companyName} | Member for {memberFor}
        </div>
        <div className="review-content">{content}</div>
        <div className="review-project">{projectName}</div>
      </Col>
    </Row>
  );
};

export default ReviewCard;
