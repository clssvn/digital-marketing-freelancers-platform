using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FreelancePlatform.Models;
using FreelancePlatform.Resources;
using FreelancePlatform.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FreelancePlatform.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FreelancersController : ControllerBase
    {
        private readonly topmContext _context;
        private readonly IMapper _mapper;
        private readonly ApiServices _apiServices;

        public FreelancersController(topmContext context, IMapper mapper, ApiServices apiServices)
        {
            _context = context;
            _mapper = mapper;
            _apiServices = apiServices;
        }

        //LISTA FREELANCERÓW - do weryfikacji
        [Authorize(Roles = "admin")]
        [HttpGet("unverified")]
        public async Task<ActionResult<IEnumerable<FreelancerResource>>> GetUnVerifiedFreelancers()
        {
            var freelancers = await _context.Freelancer.Where(f => f.IdNavigation.IsVerified == false).ToListAsync();

            if (freelancers == null) return NoContent();


            var freelancersResources = new List<FreelancerResource>();

            freelancers.ForEach(f => freelancersResources.Add(_apiServices.MapFreelancer(f)));

            return Ok(freelancersResources);
        }

        //FREELANCER - weryfikacja
        [Authorize(Roles = "admin")]
        [HttpPut("unverified/{id:int}")]
        public IActionResult GetUnVerifiedFreelancer(int id)
        {
            var uf = _context.Freelancer.FirstOrDefault(e => e.Id == id);
            if (uf == null)
            {
                return NotFound();
            }

            if (!uf.IdNavigation.IsVerified)
            {
                uf.IdNavigation.IsVerified = true;
                _context.SaveChanges();
            }
            else
            {
                return Conflict();
            }

            return Ok();
        }

        //LISTA FREELANCERÓW - aktywne
        [Authorize]
        [HttpGet("active")]
        public async Task<ActionResult<IEnumerable<FreelancerResource>>> GetActiveFreelancers()
        {

            var freelancers = await _context.Freelancer.Where(f => f.IdNavigation.IsVerified == true).ToListAsync();



            if (freelancers == null) return NoContent();

            var freelancersResources = new List<FreelancerResource>();
            freelancers.ForEach(f => freelancersResources.Add(_apiServices.MapFreelancer(f)));

            return Ok(freelancersResources);
        }

        //FREELANCER
        [Authorize]
        [HttpGet("{id:int}")]
        public async Task<ActionResult<FreelancerResource>> GetActiveFreelancer(int id)
        {
            var freelancer = await _context.Freelancer.FirstOrDefaultAsync(f => f.Id == id);

            if (freelancer == null) return NoContent();

            var freelancerResource = _mapper.Map<Freelancer, FreelancerResource>(freelancer);

            var skills = new List<SkillResource>();
            freelancer.SkillFreelancer.ToList().ForEach(s => skills.Add(_mapper.Map<Skill, SkillResource>(s.Skill)));
            freelancerResource.Skill = skills;

            return Ok(freelancerResource);
        }

    }
}