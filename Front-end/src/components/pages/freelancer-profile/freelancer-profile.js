import "./freelancer-profile.css";

import React, { Component } from "react";

import FreelancerCard from "../../freelancer-card";
import CaseList from "../../case-list";
import ReviewsList from "../../reviews-list";

import {
  fetchFreelancer,
  fetchCasesList,
  fetchReviewsList,
} from "../../../actions";
import { compose } from "redux";
import { connect } from "react-redux";
import { withFreelanceAppService } from "../../hoc";
import { Button, Image, Row } from "react-bootstrap";
import Loading from "../../loading/loading";
import Error from "../../error/error";

const Reviews = ({ reviews }) => {
  return (
    <>
      <hr style={{ color: "#acacac" }} />
      <div className="header">REVIEWS ABOUT YOU</div>
      <ReviewsList reviews={reviews} />
      <hr style={{ color: "#acacac" }} />
    </>
  );
};

const Cases = ({ cases }) => {
  return (
    <>
      <div className="justify-content-center projects card-deck">
        <CaseList cases={cases} />
      </div>
    </>
  );
};

class FreelancerProfile extends Component {
  componentDidMount() {
    this.props.fetchCasesList(this.props.id);
    this.props.fetchReviewsList(this.props.id);
    this.props.fetchFreelancer(this.props.id);
  }

  render() {
    const { freelancer, caseList, review } = this.props;

    if (freelancer.loading || caseList.loading) {
      return <Loading />;
    }
    if (freelancer.error || caseList.error) {
      return <Error />;
    }
    return (
      <div className="container-flud fl-page">
        <FreelancerCard freelancer={freelancer} />
        <h4 className="header">About me</h4>
        <div className="no-cases">{freelancer.freelancer.info}</div>
        <hr style={{ color: "#acacac" }} />
        <div className="header">YOUR PORTFOLIO</div>
        <Button href="/add-case" className="btn-add-case btn-outline-primary btn-sm">
          Add case
        </Button>

        <Cases cases={caseList} />
        <Reviews reviews={review} />
        <div className="header">Your contact</div>
        <Row className="contact-row">
          <Image
            className="contact-logo"
            src={`/images/${freelancer.freelancer.contact.type}.png`}
          />
          <div>{freelancer.freelancer.contact.contact1}</div>
        </Row>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch, { freelanceAppService }) => {
  return {
    fetchCasesList: (freelancerId) =>
      fetchCasesList(freelanceAppService, dispatch, freelancerId),
    fetchReviewsList: (freelancerId) =>
      fetchReviewsList(freelanceAppService, dispatch, freelancerId),
    fetchFreelancer: (freelancerId) =>
      fetchFreelancer(freelanceAppService, dispatch, freelancerId),
  };
};

const mapStateToProps = ({ freelancer, caseList, review }) => {
  return { freelancer, caseList, review };
};

export default compose(
  withFreelanceAppService(),
  connect(mapStateToProps, mapDispatchToProps)
)(FreelancerProfile);
