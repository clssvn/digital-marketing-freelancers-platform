﻿using System;
using System.Collections.Generic;

namespace FreelancePlatform.Models
{
    public partial class City
    {
        public City()
        {
            UserInfo = new HashSet<UserInfo>();
        }

        public int IdCity { get; set; }
        public string Name { get; set; }
        public int CountryId { get; set; }

        public virtual Country Country { get; set; }
        public virtual ICollection<UserInfo> UserInfo { get; set; }
    }
}
