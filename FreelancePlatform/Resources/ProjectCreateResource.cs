﻿using System;
using System.Collections.Generic;
using FreelancePlatform.Models;
using Microsoft.AspNetCore.Http;

namespace FreelancePlatform.Resources
{
    public class ProjectCreateResource
    {
        public string Name { get; set; }
        public int IdKlient { get; set; }
        public string Goals { get; set; }
        public string Description { get; set; }
        public string Result { get; set; }
        public decimal? Price { get; set; }

        public int NicheId { get; set; }
        public int CategoryId { get; set; }

        public virtual ICollection<IFormFile> ImageFileUpload { get; set; }

    }
}