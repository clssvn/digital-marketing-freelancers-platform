export const getCaseList = (state, action) => {
    if (state === undefined) {
        return {
            caseList: {},
            loading: true,
            error: null
        };
    }

    switch (action.type) {

         case 'FETCH_CASES_REQUEST':
            return {
                cases: [],
                loading: true,
                error: null
            };

        case 'FETCH_CASES_SUCCESS':
            return {
                cases: action.payload,
                loading: false,
                error: null
            };

        case 'FETCH_CASES_FAILURE':
            return {
                cases: [],
                loading: false,
                error: action.payload
            };

        default:
            return state.caseList;
    };
};

export const getAllCase = (state, action) => {
    if (state === undefined) {
        return {
            caseList: {},
            loading: true,
            error: null
        };
    }

    switch (action.type) {

         case 'FETCH_ALLCASES_REQUEST':
            return {
                cases: [],
                loading: true,
                error: null
            };

        case 'FETCH_ALLCASES_SUCCESS':
            return {
                cases: action.payload,
                loading: false,
                error: null
            };

        case 'FETCH_ALLCASES_FAILURE':
            return {
                cases: [],
                loading: false,
                error: action.payload
            };

        default:
            return state.caseList;
    };
};
