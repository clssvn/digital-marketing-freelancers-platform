import { getProject } from "./project";
import { getReview } from "./reviews";
import { getFreelancer, getFreelancers, getClient, getClients, getCurrentUser } from "./user";
import { getCaseList, getAllCase } from './caseList';
import { getAdminData } from './adminData';
import { getUserEditData, getUserNewData } from './userEditData';

const reducer = (state, action) => {
    return {
        userNewData: getUserNewData(state, action),
        userEditData: getUserEditData(state, action),
        adminData: getAdminData(state, action),
        currentUser: getCurrentUser(state, action),
        freelancer: getFreelancer(state, action),
        freelancers: getFreelancers(state, action),
        client: getClient(state, action),
        clients: getClients(state, action),
        project: getProject(state, action),
        review: getReview(state, action),
        caseList: getCaseList(state, action),
        cases: getAllCase(state, action)
    };
};

export default reducer;