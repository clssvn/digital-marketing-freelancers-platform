﻿using System;
using System.Collections.Generic;
using FreelancePlatform.Models;

namespace FreelancePlatform.Resources
{
    public class ProjectUpdateResource
    {
        #nullable enable
        public string? Name { get; set; }
        public string? Goals { get; set; }
        public string? Description { get; set; }
        public string? Result { get; set; }
        public decimal? Price { get; set; }
        public ICollection<ImageFile>? ImageFile { get; set; }
    }
}
