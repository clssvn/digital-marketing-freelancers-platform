import React from 'react'
import { Link } from 'react-router-dom';

export const AdminRoutingHeader = () => {
    return(
        <div className="adminRoutingHeader row">
            <Link to="/clients">
            <button className="btn btn-primary header-button">Clients</button>
            </Link>
            <Link to="/cases">
            <button className="btn btn-primary header-button">Cases</button>
            </Link>
            <Link to="/freelancers">
            <button className="btn btn-primary header-button">Freelancers</button>
            </Link>
        </div>
    )
}