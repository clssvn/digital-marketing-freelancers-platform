import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { fetchCurrentUser } from "../../../actions";
import FreelanceAppService from "../../../services/freelance-app-service";
import { withFreelanceAppService } from "../../hoc";
import { Link, Redirect } from "react-router-dom";
import {
  Container,
  Row,
  Col,
  Nav,
  Button,
  Form,
  FloatingLabel,
} from "react-bootstrap";
import "./login.scss";
import { MainHeader } from "../../header";
import { validEmail, validPassword } from "../../../regex.js";
import { toast } from "react-toastify";

class Login extends Component {
  state = {
    validated: false,
    btnDisabled: false,
    email: "",
    password: "",
  };

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleSubmit = (event) => {
    this.setState({ btnDisabled: true });
    const form = event.currentTarget;
    event.preventDefault();
    if (form.checkValidity() === false) {
      this.setState({ btnDisabled: false });
      toast.error("Please, fill all fields");
      this.setState({ btnDisabled: false });
      return;
    }

    if (!validEmail.test(this.state.email)) {
      toast.error("Please, write correct email");
      this.setState({ btnDisabled: false });
      return;
    }

    if (!validPassword.test(this.state.password)) {
      toast.error("Please, write correct password");
      this.setState({ btnDisabled: false });
      return;
    }

    event.preventDefault();
    this.props.fetchCurrentUser({
      email: this.state.email,
      password: this.state.password,
    });
    this.setState({ btnDisabled: false });
  };

  render() {
    return (
      <>
        <MainHeader />
        <div className="signin-signup-title">Sign in</div>

        <Container className="signin-signup">
          <Form
          noValidate
          validated={this.state.validated}
          onSubmit={this.handleSubmit}>
            <FloatingLabel
              controlId="floatingInput"
              label="Email address"
              className="mb-3"
            >
              <Form.Control
                required
                name="email"
                placeholder="Email"
                value={this.state.email}
                onChange={this.handleChange}
              />
            </FloatingLabel>

            <FloatingLabel
              controlId="floatingInput"
              label="Password"
              className="mb-3"
            >
              <Form.Control
                required
                type="password"
                name="password"
                placeholder="Password"
                value={this.state.password}
                onChange={this.handleChange}
              />
            </FloatingLabel>

            <Button className="signin-submit" type="submit" disabled={this.state.btnDisabled}>
              Sign in
            </Button>
          </Form>
        </Container>

        <div className="signin-signup-title">Not registrated? Sign up now</div>
        <Row className="signup-button-row">
          <Col sm={6}>
            <Link to="/signup/client" className="signup-button su-c">
              Sign up as client
            </Link>
          </Col>
          <Col sm={6}>
            <Link to="/signup/freelancer" className="signup-button su-f">
              Sign up as freelancer
            </Link>
          </Col>
        </Row>
      </>
    );
  }
}

const mapDispatchToProps = (dispatch, { freelanceAppService }) => {
  return {
    fetchCurrentUser: (userInfo) =>
      fetchCurrentUser(freelanceAppService, dispatch, userInfo),
  };
};

export default compose(
  withFreelanceAppService(),
  connect(null, mapDispatchToProps)
)(Login);
