﻿using System;
using System.Collections.Generic;

namespace FreelancePlatform.Models
{
    public partial class Freelancer
    {
        public Freelancer()
        {
            Project = new HashSet<Project>();
            Review = new HashSet<Review>();
            SkillFreelancer = new HashSet<SkillFreelancer>();
        }

        public int CategoryId { get; set; }
        public string Portfoliolink { get; set; }
        public int Id { get; set; }

        public virtual Category Category { get; set; }
        public virtual UserInfo IdNavigation { get; set; }
        public virtual ICollection<Project> Project { get; set; }
        public virtual ICollection<Review> Review { get; set; }
        public virtual ICollection<SkillFreelancer> SkillFreelancer { get; set; }
    }
}