﻿using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace FreelancePlatform.Models
{
    public partial class topmContext : IdentityDbContext<User, Role, int>
    {
        public topmContext()
        {
        }

        public topmContext(DbContextOptions<topmContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Avatar> Avatar { get; set; }
        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<City> City { get; set; }
        public virtual DbSet<Contact> Contact { get; set; }
        public virtual DbSet<Country> Country { get; set; }
        public virtual DbSet<Freelancer> Freelancer { get; set; }
        public virtual DbSet<ImageFile> ImageFile { get; set; }
        public virtual DbSet<Klient> Klient { get; set; }
        public virtual DbSet<Niche> Niche { get; set; }
        public virtual DbSet<Project> Project { get; set; }
        public virtual DbSet<Review> Review { get; set; }
        public virtual DbSet<Skill> Skill { get; set; }
        public virtual DbSet<SkillFreelancer> SkillFreelancer { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserInfo> UserInfo { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Avatar>(entity =>
            {
                entity.HasKey(e => e.IdAvatar)
                    .HasName("avatar_pk");

                entity.ToTable("avatar");

                entity.Property(e => e.IdAvatar)
                    .HasColumnName("id_avatar")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Avatarlink)
                    .IsRequired()
                    .HasColumnName("avatarlink")
                    .HasMaxLength(300);
            });

            modelBuilder.Entity<Category>(entity =>
            {
                entity.HasKey(e => e.IdCategory)
                    .HasName("category_pk");

                entity.ToTable("category");

                entity.Property(e => e.IdCategory)
                    .HasColumnName("id_category")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<City>(entity =>
            {
                entity.HasKey(e => e.IdCity)
                    .HasName("city_pk");

                entity.ToTable("city");

                entity.Property(e => e.IdCity)
                    .HasColumnName("id_city")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.CountryId).HasColumnName("country_id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(70);

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.City)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("miasto_kraj");
            });

            modelBuilder.Entity<Contact>(entity =>
            {
                entity.HasKey(e => e.IdContact)
                    .HasName("contact_pk");

                entity.ToTable("contact");

                entity.Property(e => e.IdContact)
                    .HasColumnName("id_contact")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Contact1)
                    .IsRequired()
                    .HasColumnName("contact")
                    .HasMaxLength(50);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasMaxLength(20);

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.User)
                    .WithOne(p => p.Contact)
                    .HasForeignKey<Contact>(p => p.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("kontakt_uzytkownik");
            });

            modelBuilder.Entity<Country>(entity =>
            {
                entity.HasKey(e => e.IdCountry)
                    .HasName("country_pk");

                entity.ToTable("country");

                entity.Property(e => e.IdCountry)
                    .HasColumnName("id_country")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(70);
            });

            modelBuilder.Entity<Freelancer>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("freelancer_pk");

                entity.ToTable("freelancer");

                entity.Property(e => e.Id)
                    .HasColumnName("id_user")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.CategoryId).HasColumnName("category_id");

                entity.Property(e => e.Portfoliolink)
                    .IsRequired()
                    .HasColumnName("portfoliolink")
                    .HasMaxLength(300);

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Freelancer)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("freelancer_kategoria");

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.Freelancer)
                    .HasForeignKey<Freelancer>(d => d.Id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("freelancer_user_info");
            });

            modelBuilder.Entity<ImageFile>(entity =>
            {
                entity.HasKey(e => e.IdFile)
                    .HasName("image_file_pk");

                entity.ToTable("image_file");

                entity.Property(e => e.IdFile)
                    .HasColumnName("id_file")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Filelink)
                    .IsRequired()
                    .HasColumnName("filelink")
                    .HasMaxLength(300);

                entity.Property(e => e.ProjectId).HasColumnName("project_id");

                entity.HasOne(d => d.Project)
                    .WithMany(p => p.ImageFile)
                    .HasForeignKey(d => d.ProjectId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("plik_graficzny_projekt");
            });

            modelBuilder.Entity<Klient>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("klient_pk");

                entity.ToTable("klient");

                entity.Property(e => e.Id)
                    .HasColumnName("id_user")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.CompanySize).HasColumnName("company_size");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(100);

                entity.Property(e => e.NicheId).HasColumnName("niche_id");

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.Klient)
                    .HasForeignKey<Klient>(d => d.Id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("klient_user_info");

                entity.HasOne(d => d.Niche)
                    .WithMany(p => p.Klient)
                    .HasForeignKey(d => d.NicheId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("klient_niche");
            });

            modelBuilder.Entity<Niche>(entity =>
            {
                entity.HasKey(e => e.IdNiche)
                    .HasName("niche_pk");

                entity.ToTable("niche");

                entity.Property(e => e.IdNiche)
                    .HasColumnName("id_niche")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(70);
            });

            modelBuilder.Entity<Project>(entity =>
            {
                entity.HasKey(e => e.IdProject)
                    .HasName("project_pk");

                entity.ToTable("project");

                entity.Property(e => e.IdProject)
                    .HasColumnName("id_project")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.AddedDate).HasColumnName("added_date");

                entity.Property(e => e.CategoryId).HasColumnName("category_id");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description");

                entity.Property(e => e.Goals)
                    .IsRequired()
                    .HasColumnName("goals");

                entity.Property(e => e.IdFreelancer).HasColumnName("id_freelancer");

                entity.Property(e => e.IdKlient).HasColumnName("id_klient");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50);

                entity.Property(e => e.NicheId).HasColumnName("niche_id");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("money");

                entity.Property(e => e.Result)
                    .IsRequired()
                    .HasColumnName("result");

                entity.Property(e => e.ReviewId).HasColumnName("review_id");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Project)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("projekt_kategoria");

                entity.HasOne(d => d.IdFreelancerNavigation)
                    .WithMany(p => p.Project)
                    .HasForeignKey(d => d.IdFreelancer)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("projekt_freelancer");

                entity.HasOne(d => d.IdKlientNavigation)
                    .WithMany(p => p.Project)
                    .HasForeignKey(d => d.IdKlient)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("projekt_klient");

                entity.HasOne(d => d.Niche)
                    .WithMany(p => p.Project)
                    .HasForeignKey(d => d.NicheId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("projekt_nisza");

                entity.HasOne(d => d.Review)
                    .WithOne(p => p.Project)
                    .HasForeignKey<Project>(d => d.ReviewId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("projekt_opinia");
            });

            modelBuilder.Entity<Review>(entity =>
            {
                entity.HasKey(e => e.IdReview)
                    .HasName("review_pk");

                entity.ToTable("review");

                entity.Property(e => e.IdReview)
                    .HasColumnName("id_review")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.AddedDate).HasColumnName("added_date");

                entity.Property(e => e.ClientId).HasColumnName("client_id");

                entity.Property(e => e.Content)
                    .IsRequired()
                    .HasColumnName("content");

                entity.Property(e => e.FreelancerId).HasColumnName("freelancer_id");

                //entity.HasOne(d => d.Client)
                //    .WithMany(p => p.Review)
                //    .HasForeignKey(d => d.ClientId)
                //    .OnDelete(DeleteBehavior.ClientSetNull)
                //    .HasConstraintName("opinia_klient");

                entity.HasOne(d => d.Freelancer)
                    .WithMany(p => p.Review)
                    .HasForeignKey(d => d.FreelancerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("review_freelancer");
            });

            modelBuilder.Entity<Skill>(entity =>
            {
                entity.HasKey(e => e.IdSkill)
                    .HasName("skill_pk");

                entity.ToTable("skill");

                entity.Property(e => e.IdSkill)
                    .HasColumnName("id_skill")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<SkillFreelancer>(entity =>
            {
                entity.HasKey(e => new { e.SkillId, e.FreelancerId })
                    .HasName("skill_freelancer_pk");

                entity.ToTable("skill_freelancer");

                entity.Property(e => e.SkillId).HasColumnName("skill_id");

                entity.Property(e => e.FreelancerId).HasColumnName("freelancer_id");

                entity.HasOne(d => d.Freelancer)
                    .WithMany(p => p.SkillFreelancer)
                    .HasForeignKey(d => d.FreelancerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("umietnosci_freelancer_freelancer");

                entity.HasOne(d => d.Skill)
                    .WithMany(p => p.SkillFreelancer)
                    .HasForeignKey(d => d.SkillId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("umietnosci_freelancer_umietnosci");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("user_pk");

                entity.Property(e => e.Id)
                    .HasColumnName("id_user")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50);

                entity.Property(e => e.Pass)
                    .IsRequired()
                    .HasColumnName("pass")
                    .HasMaxLength(30);

                entity.Property(e => e.Surname)
                    .IsRequired()
                    .HasColumnName("surname")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<UserInfo>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .HasName("user_info_pk");

                entity.ToTable("user_info");

                entity.Property(e => e.Id)
                    .HasColumnName("id_user")
                    .ValueGeneratedNever();

                entity.Property(e => e.AvatarIdAvatar).HasColumnName("avatar_id_avatar");

                entity.Property(e => e.CityId).HasColumnName("city_id");

                entity.Property(e => e.Info)
                    .IsRequired()
                    .HasColumnName("info");

                entity.Property(e => e.IsVerified).HasColumnName("is_verified");

                entity.Property(e => e.RegDate).HasColumnName("reg_date");

                entity.HasOne(d => d.AvatarIdAvatarNavigation)
                    .WithMany(p => p.UserInfo)
                    .HasForeignKey(d => d.AvatarIdAvatar)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("user_info_avatar");

                entity.HasOne(d => d.City)
                    .WithMany(p => p.UserInfo)
                    .HasForeignKey(d => d.CityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("user_info_city");

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.UserInfo)
                    .HasForeignKey<UserInfo>(d => d.Id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("user_info_user");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
