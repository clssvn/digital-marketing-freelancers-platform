using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FreelancePlatform.Models;
using FreelancePlatform.Resources;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FreelancePlatform.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NicheController : ControllerBase
    {
        private topmContext _context;
        private readonly IMapper _mapper;


        public NicheController(topmContext context, UserManager<User> userManager, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;

        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<NicheResource>>> GetNiches()
        {
            var niches = await _context.Niche.ToListAsync();
            var nichesResources = _mapper.Map<IEnumerable<Niche>, IEnumerable<NicheResource>>(niches).ToList();

            return Ok(nichesResources);
        }


    }
}