﻿using System;
namespace FreelancePlatform.Resources
{
    public class CategoryResource
    {
        public int IdCategory { get; set; }
        public string Name { get; set; }
    }
}
