﻿using System;
using System.Collections.Generic;
using FreelancePlatform.Models;

namespace FreelancePlatform.Resources
{
    public class FreelancerResource
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Avatar { get; set; }
        public string City { get; set; }
        public string MemberFor { get; set; }

        public ICollection<SkillResource> Skill { get; set; }
        public ContactResource Contact { get; set; }
        public string Portfoliolink { get; set; }
        public CategoryResource Category { get; set; }
        public string Info { get; set; }

    }
}
