export const getReview = (state, action) => {
    if (state === undefined) {
        return {
            review: [],
            loading: true,
            error: null
        };
    }

    switch (action.type) {
        case 'FETCH_REVIEWS_REQUEST':
            return {
                review: [],
                loading: true,
                error: null
            };
        case 'FETCH_REVIEWS_SUCCESS':
            return {
                review: action.payload,
                loading: false,
                error: null
            };
        case 'FETCH_REVIEWS_FAILURE':
            return {
                review: [],
                loading: false,
                error: action.payload
            };
        default:
            return state.review;
    };
};
