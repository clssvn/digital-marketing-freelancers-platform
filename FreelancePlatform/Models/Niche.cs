﻿using System;
using System.Collections.Generic;

namespace FreelancePlatform.Models
{
    public partial class Niche
    {
        public Niche()
        {
            Klient = new HashSet<Klient>();
            Project = new HashSet<Project>();
        }

        public int IdNiche { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Klient> Klient { get; set; }
        public virtual ICollection<Project> Project { get; set; }
    }
}
