import React, { Component } from "react";
import "./client-card.scss";
import { Row, Col, Image } from "react-bootstrap";
import Loading from "../loading/loading";
import Error from "../error/error";

export default class ClientCard extends Component {
  render() {
    const { client, loading, error } = this.props.client;

    if (loading) {
      return <Loading />;
    }
    if (error) {
      return <Error />;
    }

    const {
      avatar,
      name,
      memberFor,
      surname,
      city,
      companyName,
      niche,
    } = client;
    return (
      <>
        <div className="user-card">
          <Row>
            <Col sm={4} className="image-col">
              <Image src={avatar} className="avatar" alt="avatar" />
            </Col>
            <Col sm={8} className="data-col">
              <Row className="name">{name + " " + surname}</Row>
              <Row className="city-regdate">
                {city} | Member for {memberFor}
              </Row>
              <Row className="company-name">{companyName}</Row>
              <Row className="niche">{niche.name}</Row>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}
