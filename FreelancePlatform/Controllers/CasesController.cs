using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FreelancePlatform.Models;
using FreelancePlatform.Resources;
using FreelancePlatform.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FreelancePlatform.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CasesController : ControllerBase
    {
        private readonly UserManager<User> _userManager;
        private readonly topmContext _context;
        private readonly IMapper _mapper;
        private readonly AWSService _awsService;

        public CasesController(topmContext context, UserManager<User> userManager, IMapper mapper, AWSService awsService)
        {
            _context = context;
            _userManager = userManager;
            _mapper = mapper;
            _awsService = awsService;

        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProjectResource>>> GetCases()
        {

            var projects = await _context.Project.OrderByDescending(p => p.AddedDate).ToListAsync();
            var projectsResources = _mapper.Map<IEnumerable<Project>, IEnumerable<ProjectResource>>(projects).ToList();


            return Ok(projectsResources);
        }

        [Authorize]
        [HttpGet("user={id:int}")]
        public async Task<ActionResult<IEnumerable<ProjectResource>>> GetUserCasesAsync(int id)
        {
            if (!_context.User.Any(e => e.Id == id))
            {
                return NoContent();
            }

            var projects = await _context.Project.Where(e => e.IdFreelancer == id || e.IdKlient == id).ToListAsync();
            var projectsResources = _mapper.Map<IEnumerable<Project>, IEnumerable<ProjectResource>>(projects).ToList();

            return Ok(projectsResources);
        }

        [Authorize]
        [HttpGet("{id:int}")]
        public async Task<ActionResult<IEnumerable<ProjectResource>>> GetCase(int id)
        {
            var project = await _context.Project.FirstOrDefaultAsync(e => e.IdProject == id);

            if (project == null)
            {
                return NoContent();
            }

            var projectResource = _mapper.Map<Project, ProjectResource>(project);

            return Ok(projectResource);
        }


        [Authorize(Roles = "freelancer")]
        [HttpPost]
        public async Task<IActionResult> AddCase([FromForm] ProjectCreateResource newProject)
        {
            var currentUser = _userManager.GetUserAsync(HttpContext.User).Result;

            if (currentUser.UserInfo.IsVerified == false)
            {
                return Unauthorized();
            }

            var project = _mapper.Map<ProjectCreateResource, Project>(newProject);

            project.AddedDate = DateTime.Now;
            project.IdFreelancer = currentUser.Id;
            project.ImageFile = _awsService.CreateImageFiles(newProject.ImageFileUpload, "cases");

            await _context.Project.AddAsync(project);

            await _context.SaveChangesAsync();

            return Ok();
        }
    }
}