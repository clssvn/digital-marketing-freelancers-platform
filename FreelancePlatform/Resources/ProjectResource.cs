﻿using System;
using System.Collections.Generic;

namespace FreelancePlatform.Resources
{
    public class ProjectResource
    {
        public int IdProject { get; set; }
        public string Name { get; set; }
        public DateTime? AddedDate { get; set; }
        public string Goals { get; set; }
        public string Description { get; set; }
        public string Result { get; set; }
        public decimal? Price { get; set; }
        public int? ReviewId { get; set; }


        public CategoryResource Category { get; set; }
        public UserProjectResource Freelancer { get; set; }
        public UserProjectResource Klient { get; set; }
        public NicheResource Niche { get; set; }
        public ICollection<ImageFileResource> ImageFile { get; set; }
    }

    public class UserProjectResource
    {
        public int Id { get; set; }
        public string Avatar { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
    }

    public class ImageFileResource
    {
        public int IdFile { get; set; }
        public string Filelink { get; set; }
    }
}
