import React from 'react'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import { Home, FreelancerPage, ProjectCase, FreelancerSearch, CaseSearch, ClientSearch, ClientPage, Login, AddCase, Signup, SignupClient, AddReview } from '../pages';
import EditProfile from '../pages/edit-profile/edit-profile';
import ReviewsList from '../reviews-list';
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ConfirmPage from '../pages/signup/confirm-page';

const PrivateRoute = ({path, component}) => {
    var authed = localStorage.getItem("token") != null;

    if(authed){
        return(
            <Route exact path={path} component={component} />
        )
    }

    return (
    <Redirect to={{pathname: '/'}} />
    )
  }

  const PublicRoute = ({path, component}) => {
    var authed = localStorage.getItem("token") != null;

    if(!authed){
        return(
            <Route exact path={path} component={component} />
        )
    }

    return (
    <Redirect to={{pathname: '/'}} />
    )
  }

export const App = () => {
    return (
        <>
        <ToastContainer />
        
        <Router>
            <Switch>
                <Route exact path="/" component={Home} />
                <PublicRoute exact path="/login" component={Login} />
                <PublicRoute exact path="/signup/freelancer" component={Signup} />
                <PublicRoute exact path="/signup/client" component={SignupClient} />
                <PublicRoute exact path="/signup/confirm" component={ConfirmPage} />

                <PrivateRoute path="/freelancer/id:id" component={FreelancerPage} />
                <PrivateRoute path="/client/id:id" component={ClientPage} />
                <PrivateRoute path="/case/:id" component={ProjectCase} />
                <PrivateRoute path="/review/:id" component={ReviewsList} />
                <PrivateRoute path="/freelancers" component={FreelancerSearch} />
                <PrivateRoute path="/cases" component={CaseSearch} />
                <PrivateRoute path="/clients" component={ClientSearch} />
                <PrivateRoute exact path="/add-case" component={AddCase} />
                <PrivateRoute path="/edit-profile" component={EditProfile} />
        
                <Redirect to="/" />

        </Switch>
        </Router>
        </>
    );
}