﻿using System;
using System.Collections.Generic;

namespace FreelancePlatform.Models
{
    public partial class Category
    {
        public Category()
        {
            Freelancer = new HashSet<Freelancer>();
            Project = new HashSet<Project>();
        }

        public int IdCategory { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Freelancer> Freelancer { get; set; }
        public virtual ICollection<Project> Project { get; set; }
    }
}
