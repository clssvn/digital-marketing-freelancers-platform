﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security;
using System.Threading.Tasks;
using Amazon.S3;
using FreelancePlatform.Models;
using Microsoft.AspNetCore.Http;

namespace FreelancePlatform.Services
{
    public class AWSService
    {
        private const string AWS_accessKey = "AKIAZMNVL62CAEY3MQVL";
        private const string AWS_secretKey = "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";
        private const string AWS_bucketName = "freelanceappstorage";

        public async Task<string> UploadFileToAWSAsync(IFormFile myfile, string folder)
        {
            var s3Client = new AmazonS3Client(AWS_accessKey, AWS_secretKey, Amazon.RegionEndpoint.EUNorth1);
            var bucketName = AWS_bucketName;
            var keyName = folder + "/" + DateTime.Now.Ticks.ToString();

            try
            {

                var fs = myfile.OpenReadStream();
                var request = new Amazon.S3.Model.PutObjectRequest
                {
                    BucketName = bucketName,
                    Key = keyName,
                    InputStream = fs,
                    ContentType = myfile.ContentType,
                    CannedACL = S3CannedACL.PublicRead
                };
                Console.WriteLine(request.Key.ToString() + " - " + request.BucketName.ToString());

                await s3Client.PutObjectAsync(request);

                return string.Format("http://{0}.s3.amazonaws.com/{1}", bucketName, keyName);
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                        (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") ||
                        amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    throw new SecurityException("Invalid Amazon S3 credentials - data was not uploaded.", amazonS3Exception);
                }

                throw new HttpRequestException("Unspecified error attempting to upload data: " + amazonS3Exception.Message, amazonS3Exception);
            }

        }
             


        public ICollection<ImageFile> CreateImageFiles(ICollection<IFormFile> files, string folder)
        {
            var res = new List<ImageFile>();
            files.ToList().ForEach(
                 f =>
                 {
                     var link = UploadFileToAWSAsync(f, folder);
                     var i = new ImageFile(link.Result);
                     res.Add(i);
                 }
                );
            return res;
        }
    }

    
}
