import React from 'react'

const {
    Provider: FreelanceAppServiceProvider,
    Consumer: FreelanceAppServiceConsumer
} = React.createContext();

export {
    FreelanceAppServiceProvider,
    FreelanceAppServiceConsumer
};