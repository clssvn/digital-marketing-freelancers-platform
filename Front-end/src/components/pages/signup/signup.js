import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { fetchCurrentUser } from "../../../actions";
import { withFreelanceAppService } from "../../hoc";
import AsyncSelect from "react-select/async";
import makeAnimated from "react-select/animated";
import { Button, Col, Form, Row } from "react-bootstrap";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "./signup.scss";
import { MainHeader } from "../../header";
import configData from "../../../config.json";
import { validEmail, validPassword } from "../../../regex.js";

const _apiBase = configData.SERVER_URL;
const animatedComponents = makeAnimated();

const contactTypes = [
  { id: 1, name: "Instagram" },
  { id: 2, name: "Facebook" },
  { id: 3, name: "LinkedIn" },
  { id: 4, name: "WhatsApp" },
  { id: 5, name: "E-mail" }
];

const postResource = async (data) => {
  const res = await fetch(`${_apiBase}Auth/signup/freelancer`, {
    method: "POST",
    body: data,
  });

  if (!res.ok) {
    toast.error("Something went wrong. Please try again");
  }
};

class Signup extends Component {
  state = {
    btnDisabled: false,
    validated1: false,
    validated2: false,

    query: "",
    step: false,

    email: "",
    name: "",
    surname: "",
    pass: "",
    avatar: {},

    city: "",
    country: "",

    info: "",

    contactId: "",
    contact: "",

    categoryId: "",
    portfolio: {},
    skills: [],
  };

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  uploadFiles = (event) => {
    this.setState({
      [event.target.name]: event.target.files[0],
    });
  };

  handleSubmit = async (event) => {
    this.setState({btnDisabled: true})
    const form = event.currentTarget;
    if (
      form.checkValidity() === false ||
      this.state.city === "" ||
      this.state.country === "" ||
      this.state.contactId === "" ||
      this.state.skills === []
    ) {
      event.preventDefault();
      event.stopPropagation();
      toast.error("Please, fill all required fields");
      this.setState({btnDisabled: false})

      return;
    }

    this.setState({ validated2: true });

    event.preventDefault();

    const formData = new FormData();

    formData.append("UserSignUpResource.Email", this.state.email);
    formData.append("UserSignUpResource.name", this.state.name);
    formData.append("UserSignUpResource.surname", this.state.surname);
    formData.append("UserSignUpResource.pass", this.state.pass);
    formData.append("UserInfoSignUpResource.avatar", this.state.avatar);
    formData.append(
      "UserInfoSignUpResource.CityResource.Name",
      this.state.city
    );
    formData.append(
      "UserInfoSignUpResource.CityResource.CountryResource.Name",
      this.state.country
    );
    formData.append("UserInfoSignUpResource.Info", this.state.info);

    formData.append(
      "FreelancerSignUpDataResource.categoryId",
      this.state.categoryId
    );
    formData.append(
      "FreelancerSignUpDataResource.portfolio",
      this.state.portfolio
    );

    const sf = this.state.skills;

    for (let i = 0; i < sf.length; i++) {
      formData.append(`SkillFreelancer[${i}].IdSkill`, sf[i].idSkill);
    }

    formData.append(
      "UserSignUpResource.ContactResource.Type",
      this.state.contactId
    );
    formData.append(
      "UserSignUpResource.ContactResource.Contact1",
      this.state.contact
    );

    var res = await postResource(formData);
    if(res.ok){
    this.props.history.push("/signup/confirm");
    }

    if(!res.ok){
      toast.error("Something went wrong. Please, try again")
  }
  };

  loadOptionsLocation = async () => {
    if (this.state.query.trim() === "") return;

    const res = await fetch(
      `https://wft-geo-db.p.rapidapi.com/v1/geo/cities?namePrefix=${this.state.query}&sort=-population`,
      {
        method: "GET",
        headers: {
          "x-rapidapi-key":
            "83325b42d2msh989a2969abed419p11a09fjsn63352cbeb83d",
          "x-rapidapi-host": "wft-geo-db.p.rapidapi.com",
        },
      }
    );
    if (!res.ok) {
      toast.error("Something went wrong. Please try again");
    }
    return await res.json().then((result) => {
      return result.data;
    });
  };

  loadOptionsCategory = async () => {
    const res = await fetch(`${_apiBase}Category`);

    if (!res.ok) {
      throw new Error(`Could not fetch, received ${res.status}`);
    }
    return await res.json().then((result) => {
      return result;
    });
  };

  loadOptionsSkills = async () => {
    const res = await fetch(`${_apiBase}Skills`);

    if (!res.ok) {
      throw new Error(`Could not fetch, received ${res.status}`);
    }
    return await res.json().then((result) => {
      return result;
    });
  };

  loadOptionsContacts = async () => {
    return contactTypes;
  };

  changeStep = (event) => {
    event.preventDefault();

    const form = event.currentTarget;
    if (form.checkValidity() === false || this.state.categoryId === "") {
      event.stopPropagation();
      toast.error("Please, fill all fields");
      return;
    }

    if (!validEmail.test(this.state.email)) {
      toast.error("Please, write correct email");
      this.setState({ btnDisabled: false });
      return;
    }

    if (!validPassword.test(this.state.pass)) {
      toast.error(`Password must contain at least 8 characters but no longer than 30 characters. At least one digit, one letter and one nonalphanumeric character.`);
      this.setState({ btnDisabled: false });
      return;
    }

    event.preventDefault();
    this.setState({ validated1: true });
    this.setState({ step: true });
  };

  render() {
    if (!this.state.step) {
      return (
        <>
          <MainHeader />

          <div className="signin-signup-title">
            Apply to join us as freelancer
          </div>
          <Form
            noValidate
            validated={this.state.validated1}
            className="signup-form"
            onSubmit={this.changeStep}
          >
            <Row className="signup-row">
              <Col className="signup-col">
                <Form.Control
                  required
                  type="text"
                  className="form-control"
                  name="name"
                  placeholder="Name"
                  value={this.state.name}
                  onChange={this.handleChange}
                />

                <Form.Control
                  required
                  type="text"
                  className="form-control"
                  name="surname"
                  placeholder="Surname"
                  value={this.state.surname}
                  onChange={this.handleChange}
                />

                <AsyncSelect
                  cacheOptions
                  components={animatedComponents}
                  defaultOptions
                  className="asyncselect"
                  loadOptions={this.loadOptionsCategory}
                  placeholder="Category"
                  getOptionLabel={(e) => e.name}
                  getOptionValue={(e) => e.idCategory}
                  onInputChange={(value) => this.setState({ query: value })}
                  onChange={(value) =>
                    this.setState({ query: "", categoryId: value.idCategory })
                  }
                />
              </Col>
              <Col className="signup-col">
                <Form.Control
                  required
                  name="email"
                  placeholder="Email"
                  value={this.state.email}
                  onChange={this.handleChange}
                />

                <Form.Control
                  required
                  type="password"
                  name="pass"
                  placeholder="Password"
                  value={this.state.pass}
                  onChange={this.handleChange}
                />
                <Button type="submit" className="signup-submit">
                  Next
                </Button>
              </Col>
            </Row>
          </Form>
        </>
      );
    }

    if (this.state.step) {
      return (
        <>
          <MainHeader />

          <div className="signin-signup-title">
            Apply to join us as freelancer
          </div>
          <Form
            className="signup-form"
            noValidate
            validated={this.state.validated2}
            onSubmit={this.handleSubmit}
          >
            <Row className="signup-row">
              <Col className="signup-col">
                <Form.Group as={Row} className="mb-3">
                  <Form.Label column sm={4}>
                    Profile photo
                  </Form.Label>
                  <Col sm={8}>
                    <Form.Control
                      required
                      name="avatar"
                      type="file"
                      accept="image/*"
                      onChange={this.uploadFiles}
                    />
                  </Col>
                </Form.Group>

                <Form.Group as={Row} className="mb-3">
                  <Form.Label column sm={4}>
                    Portfolio
                  </Form.Label>
                  <Col sm={8}>
                    <Form.Control
                      required
                      name="portfolio"
                      type="file"
                      className="form-control"
                      accept="application/pdf"
                      onChange={this.uploadFiles}
                    />
                  </Col>
                </Form.Group>

                <Form.Group as={Row} className="mb-3">
                  <Form.Label column sm={4}>
                    Location
                  </Form.Label>
                  <Col sm={8}>
                    <AsyncSelect
                      cacheOptions
                      className="asyncselect"
                      components={animatedComponents}
                      defaultOptions
                      loadOptions={this.loadOptionsLocation}
                      placeholder="Your city"
                      getOptionLabel={(e) => `${e.name} (${e.country})`}
                      getOptionValue={(e) => e.idLocation}
                      onInputChange={(value) => this.setState({ query: value })}
                      onChange={(value) => {
                        console.log(value);
                        this.setState({
                          query: "",
                          city: value.name,
                          country: value.country,
                        });
                      }}
                    />
                  </Col>
                </Form.Group>

                <Form.Group as={Row} className="mb-3">
                  <Form.Label column sm={4}>
                    Skills
                  </Form.Label>
                  <Col sm={8}>
                    <AsyncSelect
                      className="asyncselect"
                      cacheOptions
                      components={animatedComponents}
                      defaultOptions
                      isMulti
                      loadOptions={this.loadOptionsSkills}
                      placeholder="Skills"
                      getOptionLabel={(e) => e.name}
                      getOptionValue={(e) => e.idSkill}
                      onInputChange={(value) => this.setState({ query: value })}
                      onChange={(value) => {
                        var ids = [];
                        value.forEach((v) => ids.push({ idSkill: v.idSkill }));
                        this.setState({ query: "", skills: ids });
                      }}
                    />
                  </Col>
                </Form.Group>
                <Form.Group as={Row} className="mb-3">
                  <Col className="select-contact-type" sm={4}>
                    <AsyncSelect
                      className="asyncselect"
                      cacheOptions
                      components={animatedComponents}
                      defaultOptions
                      loadOptions={this.loadOptionsContacts}
                      placeholder="Contact type"
                      getOptionLabel={(e) => e.name}
                      getOptionValue={(e) => e.id}
                      onChange={(value) =>
                        this.setState({ contactId: value.id })
                      }
                    />
                  </Col>
                  <Col sm={8}>
                    <Form.Control
                      required
                      name="contact"
                      placeholder="Contact"
                      value={this.state.contact}
                      onChange={this.handleChange}
                    />
                  </Col>
                </Form.Group>

                <Form.Group as={Row} className="mb-3">
                  <Form.Label column sm={4}>
                    About me
                  </Form.Label>
                  <Col sm={8}>
                    <Form.Control
                      required
                      as="textarea"
                      name="info"
                      placeholder="Info"
                      rows={3}
                      value={this.state.about}
                      onChange={this.handleChange}
                    />
                  </Col>
                </Form.Group>

                <Button type="submit" disabled={this.state.btnDisabled} className="signup-submit">
                  Submit
                </Button>
                <Button
                  className="signup-submit back"
                  onClick={() => this.setState({ step: false })}
                >
                  Back
                </Button>
              </Col>
            </Row>
          </Form>
        </>
      );
    }
  }
}

const mapDispatchToProps = (dispatch, { freelanceAppService }) => {
  return {
    fetchCurrentUser: (userInfo) =>
      fetchCurrentUser(freelanceAppService, dispatch, userInfo),
  };
};

export default compose(
  withFreelanceAppService(),
  connect(null, mapDispatchToProps)
)(Signup);
