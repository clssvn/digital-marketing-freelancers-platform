import React, { Component, Fragment, useState } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { compose } from "redux";
import { fetchProject, onUnload } from "../../../actions";
import FreelanceAppService from "../../../services/freelance-app-service";
import { withFreelanceAppService } from "../../hoc";
import "./project-case.scss";
import { Container, Row, Col, Image, Modal } from "react-bootstrap";
import { MainHeader } from "../../header";
import Loading from "../../loading/loading";
import Error from "../../error/error";

const ScaleImage = ({img}) => {

    const [show, setShow] = useState(true);

    const onHide = () => {setShow(false)};
    return (
      <Modal
        fullscreen={true}
        show={show}
        onHide={onHide}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
        </Modal.Header>
        <Modal.Body>
         <Image src={img} />
        </Modal.Body>
        
      </Modal>
    );

}

const ProjectInfo = ({
  freelancer: { id: fId, avatar: fAvatar, name: fName, surname: fSurname },
  klient: { id: kId, avatar: kAvatar, name: kName, surname: kSurname },
  category: { idCategory, name: cName },
  niche: { idNiche, name: nName },
  price,
}) => {
  const Price = () => {
    if (price == null) {
      return null;
    }

    return (
      <Row className="project-price">
        <Col>Price:</Col>
        <Col>{price} PLN</Col>
      </Row>
    );
  };

  return (
    <Col sm={4} className="project-info">
      <Link to={`/freelancer/id${fId}`}>
        <UserCard photo={fAvatar} name={fName + " " + fSurname} role="Author" />
      </Link>

      <Link to={`/client/id${kId}`}>
        <UserCard photo={kAvatar} name={kName + " " + kSurname} role="Client" />
      </Link>

      <Row className="project-category">
        <div>{cName}</div>
      </Row>
      <Row className="project-niche">
        <div>{nName}</div>
      </Row>
      <Price />
    </Col>
  );
};

const UserCard = ({ photo, name, role }) => {
  return (
    <Row className="user-project-card">
      <Col sm={4} xs={4} className="avatar-role">
        <div>
          <Image className="user-project-avatar" src={photo} />
          <div className="user-project-role">{role}</div>
        </div>
      </Col>
      <Col sm={8} xs={8} className="user-project-name">
        {name}
      </Col>
    </Row>
  );
};

const DescriptionLine = (props) => {
  return (
    <>
      <h4 className="project-data-title">{props.title}</h4>
      <div className="project-data">{props.children}</div>
    </>
  );
};

const ProjectDescription = ({ goals, description, results }) => {
  return (
    <Col sm={8} className="project-desc">
      <DescriptionLine title="Goals">{goals}</DescriptionLine>
      <DescriptionLine title="Description">{description}</DescriptionLine>
      <DescriptionLine title="Results">{results}</DescriptionLine>
    </Col>
  );
};

class ProjectCase extends Component {
  componentDidMount() {
    this.props.fetchProject(this.props.match.params.id);
  }

  componentUnmount() {
    this.props.onUnload();
  }

  render() {
    const { loading, error } = this.props.project;

    if (loading) {
      return <Loading />;
    }

    if (error) {
      return <Error />;
    }

    const {
      freelancer,
      klient,
      category,
      niche,
      goals,
      description,
      result,
      name,
      price,
      imageFile,
    } = this.props.project.project;

    return (
      <>
        <MainHeader />
        <div className="project-case">
          <Row className="project-name">{name}</Row>
          <hr style={{ color: "#acacac" }} />
          <Row>
            <ProjectInfo
              freelancer={freelancer}
              klient={klient}
              category={category}
              niche={niche}
              price={price}
            />

            <ProjectDescription
              goals={goals}
              description={description}
              results={result}
            />
          </Row>


          <Row className="project-img">
            
              {imageFile.map((pic) => {
                return <Image src={pic.filelink} />;
              })}
           
          </Row>
        </div>
      </>
    );
  }
}


const mapDispatchToProps = (dispatch, { freelanceAppService }) => {
  return {
    fetchProject: (id) => fetchProject(freelanceAppService, dispatch, id),
    onUnload: () => onUnload(dispatch),
  };
};

const mapStateToProps = ({ project }) => {
  return { project };
};

export default compose(
  withFreelanceAppService(),
  connect(mapStateToProps, mapDispatchToProps)
)(ProjectCase);
