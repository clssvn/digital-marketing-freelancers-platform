using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FreelancePlatform.Models;
using FreelancePlatform.Resources;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FreelancePlatform.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReviewsController : ControllerBase
    {
        private topmContext _context;
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;


        public ReviewsController(topmContext context, UserManager<User> userManager, IMapper mapper)
        {
            _context = context;
            _userManager = userManager;
            _mapper = mapper;

        }

        //OPINII FREELANCERA
        [Authorize]
        [HttpGet("freelancer={id:int}")]
        public async Task<ActionResult<IEnumerable<ReviewResource>>> GetFreelancerReviews(int id)
        {
            if (!_context.Freelancer.Any(e => e.Id == id))
            {
                return NoContent();
            }

            var reviews = await _context.Review.Where(e => e.FreelancerId == id).ToListAsync();

            var reviewsResources = _mapper.Map<IEnumerable<Review>, IEnumerable<ReviewResource>>(reviews).ToList();

            return Ok(reviewsResources);
        }

        [Authorize]
        [HttpGet("{id:int}")]
        public async Task<ActionResult<ReviewResource>> GetReview(int id)
        {
            if (!_context.Review.Any(e => e.IdReview == id))
            {
                return NoContent();
            }

            var review = await _context.Review.FirstOrDefaultAsync(e => e.IdReview == id);
            var reviewResource = _mapper.Map<Review, ReviewResource>(review);

            return Ok(reviewResource);
        }

        [Authorize(Roles = "client")]
        [HttpPost]
        public async Task<ActionResult> AddReview(ReviewCreateResource reviewCreateResource)
        {
            var newReview = _mapper.Map<ReviewCreateResource, Review>(reviewCreateResource);

            var p = await _context.Project.FirstOrDefaultAsync(p => p.IdProject == reviewCreateResource.ProjectId);
            if (p == null) return NotFound();


            newReview.ClientId = p.IdKlient;
            newReview.AddedDate = DateTime.Now;
            newReview.FreelancerId = p.IdFreelancer;

            newReview.Project = p;
            await _context.Review.AddAsync(newReview);
            await _context.SaveChangesAsync();

            return Ok();
        }

    }
}