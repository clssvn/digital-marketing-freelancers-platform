﻿using System;
using System.Collections.Generic;

namespace FreelancePlatform.Models
{
    public partial class Review
    {

        public int IdReview { get; set; }
        public DateTime AddedDate { get; set; }
        public string Content { get; set; }
        public int ClientId { get; set; }
        public int FreelancerId { get; set; }

        public virtual Klient Client { get; set; }
        public virtual Freelancer Freelancer { get; set; }
        public virtual Project Project { get; set; }
    }
}
