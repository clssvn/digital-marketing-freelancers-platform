﻿using System;
using AutoMapper;
using FreelancePlatform.Models;
using FreelancePlatform.Resources;
using FreelancePlatform.Services;

namespace FreelancePlatform.Mappings
{
    public class MappingProfile : Profile
    {
        private readonly ApiServices _apiService = new ApiServices();

        public MappingProfile()
        {

            CreateMap<Freelancer, FreelancerResource>()
                .ForMember(k => k.Id, opt => opt.MapFrom(s => s.IdNavigation.Id))
                .ForMember(k => k.Name, opt => opt.MapFrom(s => s.IdNavigation.IdNavigation.Name))
                .ForMember(k => k.Surname, opt => opt.MapFrom(s => s.IdNavigation.IdNavigation.Surname))
                .ForMember(k => k.MemberFor, opt => opt.MapFrom(s => _apiService.GetRegDateDiff(s.IdNavigation.RegDate)))
                .ForMember(k => k.Info, opt => opt.MapFrom(s => s.IdNavigation.Info))
                .ForMember(k => k.City, opt => opt.MapFrom(s => s.IdNavigation.City.Name + ", " + s.IdNavigation.City.Country.Name))
                .ForMember(k => k.Avatar, opt => opt.MapFrom(s => s.IdNavigation.AvatarIdAvatarNavigation.Avatarlink))
                .ForMember(k => k.Contact, opt => opt.MapFrom(s => s.IdNavigation.IdNavigation.Contact))
                ;


            CreateMap<Klient, KlientQueryResource>()
                .ForMember(k => k.Id, opt => opt.MapFrom(s => s.IdNavigation.Id))
                .ForMember(k => k.FullName, opt => opt.MapFrom(s => s.IdNavigation.IdNavigation.Name
                            + " " + s.IdNavigation.IdNavigation.Surname));

            CreateMap<Klient, KlientResource>()
                .ForMember(k => k.Id, opt => opt.MapFrom(s => s.IdNavigation.Id))
                .ForMember(k => k.Name, opt => opt.MapFrom(s => s.IdNavigation.IdNavigation.Name))
                .ForMember(k => k.Surname, opt => opt.MapFrom(s => s.IdNavigation.IdNavigation.Surname))
                .ForMember(k => k.CompanyName, opt => opt.MapFrom(s => s.Name))
                .ForMember(k => k.MemberFor, opt => opt.MapFrom(s => _apiService.GetRegDateDiff(s.IdNavigation.RegDate)))
                .ForMember(k => k.Info, opt => opt.MapFrom(s => s.IdNavigation.Info))
                .ForMember(k => k.City, opt => opt.MapFrom(s => s.IdNavigation.City.Name + ", " + s.IdNavigation.City.Country.Name))
                .ForMember(k => k.Avatar, opt => opt.MapFrom(s => s.IdNavigation.AvatarIdAvatarNavigation.Avatarlink))
                .ForMember(k => k.Contact, opt => opt.MapFrom(s => s.IdNavigation.IdNavigation.Contact));

            CreateMap<Contact, ContactResource>();

            CreateMap<ContactResource, Contact>();

            CreateMap<Niche, NicheResource>();

            CreateMap<UserInfoSignUpResource, UserInfo>()
                .ForMember(k => k.Info, opt => opt.MapFrom(s => s.Info));

            CreateMap<Project, ProjectResource>()
                .ForMember(k => k.Freelancer, opt => opt.MapFrom(s => s.IdFreelancerNavigation))
                .ForMember(k => k.Klient, opt => opt.MapFrom(s => s.IdKlientNavigation))
                .ForMember(k => k.ImageFile, opt => opt.MapFrom(s => s.ImageFile));

            CreateMap<ProjectCreateResource, Project>();

            CreateMap<ImageFile, ImageFileResource>();

            CreateMap<Review, ReviewResource>()
                .ForMember(k => k.Klient, opt => opt.MapFrom(s => s.Client));

            CreateMap<KlientSignUpDataResource, Klient>();

            CreateMap<Klient, KlientReviewResource>()
                .ForMember(k => k.Id, opt => opt.MapFrom(s => s.IdNavigation.Id))
                .ForMember(k => k.Name, opt => opt.MapFrom(s => s.IdNavigation.IdNavigation.Name))
                .ForMember(k => k.Surname, opt => opt.MapFrom(s => s.IdNavigation.IdNavigation.Surname))
                .ForMember(k => k.CompanyName, opt => opt.MapFrom(s => s.Name))
                .ForMember(k => k.MemberFor, opt => opt.MapFrom(s => _apiService.GetRegDateDiff(s.IdNavigation.RegDate)))
                .ForMember(k => k.Avatar, opt => opt.MapFrom(s => s.IdNavigation.AvatarIdAvatarNavigation.Avatarlink));


            CreateMap<Project, ProjectReviewResource>()
                .ForMember(k => k.IdProject, opt => opt.MapFrom(s => s.IdProject))
                .ForMember(k => k.Name, opt => opt.MapFrom(s => s.Name));

            CreateMap<UserInfo, UserInfoResource>();

            CreateMap<Skill, SkillResource>();

            CreateMap<SkillFreelancer, SkillFreelancerResource>()
                .ForMember(k => k.IdSkill, opt => opt.MapFrom(s => s.SkillId));

            CreateMap<SkillFreelancerResource, SkillFreelancer>()
                .ForMember(k => k.SkillId, opt => opt.MapFrom(s => s.IdSkill));

            CreateMap<Category, CategoryResource>();

            CreateMap<Freelancer, UserProjectResource>()
                .ForMember(k => k.Id, opt => opt.MapFrom(s => s.IdNavigation.IdNavigation.Id))
                .ForMember(k => k.Avatar, opt => opt.MapFrom(s => s.IdNavigation.AvatarIdAvatarNavigation.Avatarlink))
                .ForMember(k => k.Name, opt => opt.MapFrom(s => s.IdNavigation.IdNavigation.Name))
                .ForMember(k => k.Surname, opt => opt.MapFrom(s => s.IdNavigation.IdNavigation.Surname));

            CreateMap<Klient, UserProjectResource>()
                .ForMember(k => k.Id, opt => opt.MapFrom(s => s.IdNavigation.IdNavigation.Id))
                .ForMember(k => k.Avatar, opt => opt.MapFrom(s => s.IdNavigation.AvatarIdAvatarNavigation.Avatarlink))
                .ForMember(k => k.Name, opt => opt.MapFrom(s => s.IdNavigation.IdNavigation.Name))
                .ForMember(k => k.Surname, opt => opt.MapFrom(s => s.IdNavigation.IdNavigation.Surname));

            CreateMap<FreelancerSignUpDataResource, Freelancer>();

            CreateMap<UserSignUpResource, User>()
            .ForMember(u => u.UserName, opt => opt.MapFrom(ur => ur.Email));

            CreateMap<CityResource, City>();
            CreateMap<CountryResource, Country>();

            CreateMap<ReviewCreateResource, Review>();

            CreateMap<User, UserDataForEditResource>()
                .ForMember(u => u.Avatar, opt => opt.MapFrom(s => s.UserInfo.AvatarIdAvatarNavigation.Avatarlink))
                .ForMember(u => u.Info, opt => opt.MapFrom(s => s.UserInfo.Info));

        }
    }
}
