import React from 'react';
import { Container, Spinner } from 'react-bootstrap';
import './loading.css';

const Loading = () => {
    return(
        <Container className="loading-c">
    <Spinner className="loading" animation="border" />
    </Container>
    )
}


export default Loading;