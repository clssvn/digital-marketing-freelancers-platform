﻿using System.Collections.Generic;
using FreelancePlatform.Models;
using Microsoft.AspNetCore.Http;

namespace FreelancePlatform.Resources
{
    public class UserSignUpResource
    {
        public string Email { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string Pass { get; set; }

        public ContactResource ContactResource { get; set; }

    }
}
