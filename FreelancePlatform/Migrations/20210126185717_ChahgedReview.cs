﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FreelancePlatform.Migrations
{
    public partial class ChahgedReview : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_project_review_id",
                table: "project");

            migrationBuilder.AlterColumn<int>(
                name: "review_id",
                table: "project",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<decimal>(
                name: "price",
                table: "project",
                type: "money",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "money");

            migrationBuilder.CreateIndex(
                name: "IX_project_review_id",
                table: "project",
                column: "review_id",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_project_review_id",
                table: "project");

            migrationBuilder.AlterColumn<int>(
                name: "review_id",
                table: "project",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "price",
                table: "project",
                type: "money",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "money",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_project_review_id",
                table: "project",
                column: "review_id");
        }
    }
}
