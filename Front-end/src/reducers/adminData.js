export const getAdminData = (state, action) => {
    if (state === undefined) {
        return {
            result: {},
            loading: true,
            error: null
        };
    }

    switch (action.type) {

        case 'VERIFY_USER_REQUEST':
           return {
               result: {},
               loading: true,
               error: null
           };


        case 'VERIFY_USER_SUCCESS':
            return {
                result: action.payload,
               loading: false,
               error: null
            };

        case 'VERIFY_USER_FAILURE':
            return {
                cases: [],
                loading: false,
                error: action.payload
            };

        default:
            return state.adminData;
    };
}