const getFreelancer = (state, action) => {
    if (state === undefined) {
        return {
            freelancer: {},
            loading: true,
            error: null
        };
    }

    switch (action.type) {
        case 'FETCH_FREELANCER_REQUEST':
            return {
                freelancer: {},
                loading: true,
                error: null
            };
        case 'FETCH_FREELANCER_SUCCESS':
            return {
                freelancer: action.payload,
                loading: false,
                error: null
            };
        case 'FETCH_FREELACER_FAILURE':
            return {
                freelancer: {},
                loading: false,
                error: action.payload
            };
        default:
            return state.freelancer;
    };
};

const getFreelancers = (state, action) => {
    if (state === undefined) {
        return {
            freelancers: {},
            loading: true,
            error: null
        };
    }

    switch (action.type) {
        case 'FETCH_FREELANCERS_REQUEST':
            return {
                freelancers: {},
                loading: true,
                error: null
            };
        case 'FETCH_FREELANCERS_SUCCESS':
            return {
                freelancers: action.payload,
                loading: false,
                error: null
            };
        case 'FETCH_FREELANCERS_FAILURE':
            return {
                freelancers: {},
                loading: false,
                error: action.payload
            };
        default:
            return state.freelancers;
    };
};

const getClient = (state, action) => {
    if (state === undefined) {
        return {
            client: {},
            loading: true,
            error: null
        };
    }

    switch (action.type) {
        case 'FETCH_CLIENT_REQUEST':
            return {
                client: {},
                loading: true,
                error: null
            };
        case 'FETCH_CLIENT_SUCCESS':
            return {
                client: action.payload,
                loading: false,
                error: null
            };
        case 'FETCH_CLIENT_FAILURE':
            return {
                client: {},
                loading: false,
                error: action.payload
            };
        default:
            return state.client;
    };
};

const getClients = (state, action) => {
    if (state === undefined) {
        return {
            clients: {},
            loading: true,
            error: null
        };
    }

    switch (action.type) {
        case 'FETCH_CLIENTS_REQUEST':
            return {
                clients: {},
                loading: true,
                error: null
            };
        case 'FETCH_CLIENTS_SUCCESS':
            return {
                clients: action.payload,
                loading: false,
                error: null
            };
        case 'FETCH_CLIENTS_FAILURE':
            return {
                clients: {},
                loading: false,
                error: action.payload
            };
        default:
            return state.clients;
    };
};

const getCurrentUser = (state, action) => {
    if (state === undefined) {
        return {
            currentUser: {},
            loading: true,
            error: null
        };
    }

    switch (action.type) {
        case 'FETCH_CURRENT_USER_REQUEST':
            return {
                currentUser: {},
                loading: true,
                error: null
            };
        case 'FETCH_CURRENT_USER_SUCCESS':
            return {
                currentUser: action.payload,
                loading: false,
                error: null
            };
        case 'FETCH_CURRENT_USER_FAILURE':
            return {
                currentUser: {},
                loading: false,
                error: action.payload
            };




        case 'FETCH_CURRENT_USER_INFO_REQUEST':
            return {
                currentUser: {},
                loading: true,
                error: null
            };
        case 'FETCH_CURRENT_USER_INFO_SUCCESS':
            return {
                currentUser: action.payload,
                loading: false,
                error: null
            };
        case 'FETCH_CURRENT_USER_INFO_FAILURE':
            return {
                currentUser: {},
                loading: false,
                error: action.payload
            };

        case 'FETCH_CURRENT_LOGOUT_SUCCESS':
            return {
                currentUser: {},
                loading: false,
                error: null
            };
        default:
            return state.currentUser;
    };
};

export {
    getCurrentUser,
    getFreelancer,
    getFreelancers,
    getClient,
    getClients
}