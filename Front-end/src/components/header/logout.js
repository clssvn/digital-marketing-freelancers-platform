import React, {Component} from 'react';
import { compose } from "redux";
import { connect } from "react-redux";
import { logout } from '../../actions';
import { withFreelanceAppService } from "../hoc";
import {
  Nav
} from "react-bootstrap";


class Logout extends Component {


    handleClick = event => {
        event.preventDefault()
        localStorage.removeItem("token")
        this.props.logout()
        window.location.reload()
      }

  render() {
    return (
      <Nav.Item> 
        <Nav.Link onClick={this.handleClick} className="logout">Logout</Nav.Link>
      </Nav.Item>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
    return {
      logout: () => logout(dispatch)
    };
  };
  
  export default compose(
    withFreelanceAppService(),
    connect(null, mapDispatchToProps)
  )(Logout);