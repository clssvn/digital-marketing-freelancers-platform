﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FreelancePlatform.Models;
using FreelancePlatform.Resources;
using FreelancePlatform.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;


namespace FreelancePlatform.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : Controller
    {
        private readonly topmContext _context;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        private readonly ApiServices _apiServices;
        private readonly AWSService _awsService;


        public UserController(topmContext context, IMapper mapper, UserManager<User> userManager, ApiServices apiServices, AWSService awsService, SignInManager<User> signInManager)
        {
            _context = context;
            _mapper = mapper;
            _userManager = userManager;
            _apiServices = apiServices;
            _awsService = awsService;
            _signInManager = signInManager;
        }

        [Authorize(Roles = "freelancer,client")]
        [HttpGet("edit")]
        public async Task<ActionResult<UserDataForEditResource>> GetUserDataForEdit()
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            var userDataForEditResource = _mapper.Map<User, UserDataForEditResource>(user);
            var role = await _userManager.GetRolesAsync(user);
            if (role.FirstOrDefault() == "freelancer")
            {
                userDataForEditResource.Skill = _apiServices.MapFreelancerSkills(user.UserInfo.Freelancer.SkillFreelancer);
            };

            return userDataForEditResource;

        }

        [Authorize(Roles = "freelancer,client")]
        [HttpPost("edit/data")]
        public async Task<ActionResult> EditUserData(UserDataEditedResource userDataEditedResource)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            var role = await _userManager.GetRolesAsync(user);

            if (userDataEditedResource.Contact != null)
            {
                user.Contact.Type = userDataEditedResource.Contact.Type;
                user.Contact.Contact1 = userDataEditedResource.Contact.Contact1;
            }
            if (userDataEditedResource.Info != null)
            {
                user.UserInfo.Info = userDataEditedResource.Info;
            }
            if (userDataEditedResource.SkillFreelancerResource != null && role.FirstOrDefault() == "freelancer")
            {
                _context.SkillFreelancer.RemoveRange(user.UserInfo.Freelancer.SkillFreelancer);
                user.UserInfo.Freelancer.SkillFreelancer = _apiServices.MapSkillFreelancer(userDataEditedResource.SkillFreelancerResource);
            }

            _context.SaveChanges();

            return Ok();

        }

        [Authorize(Roles = "freelancer,client")]
        [HttpPost("edit/avatar")]
        public async Task<ActionResult> EditUserAvatar([FromForm] FileResource avatar)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            var newAvatar = await _awsService.UploadFileToAWSAsync(avatar.File, "avatar");

            user.UserInfo.AvatarIdAvatarNavigation.Avatarlink = newAvatar;

            _context.SaveChanges();


            return Ok();

        }

        [Authorize(Roles = "freelancer,client")]
        [HttpPost("edit/pass")]
        public async Task<ActionResult> EditUserPass(ChangePassResource passResource)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            var result = await _userManager.ChangePasswordAsync(user, passResource.Current, passResource.New);

            if (result.Succeeded)
            {
                await _userManager.UpdateAsync(user);
                user.Pass = passResource.New;
                await _context.SaveChangesAsync();
                return Ok();
            }
            else
            {
                return Conflict(result.Errors);
            }

        }

    }
}
