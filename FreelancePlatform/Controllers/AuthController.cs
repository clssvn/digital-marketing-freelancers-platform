﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FreelancePlatform.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using FreelancePlatform.Resources;
using FreelancePlatform.Settings;
using Microsoft.Extensions.Options;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using FreelancePlatform.Services;

namespace FreelancePlatform.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {

        private readonly topmContext _context;
        private readonly ApiServices _apiService;
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;
        //private readonly RoleManager<Role> _roleManager;
        private readonly JwtSettings _jwtSettings;
        public readonly AWSService _awsService;

        public AuthController(IMapper mapper, UserManager<User> userManager,
            RoleManager<Role> roleManager, IOptionsSnapshot<JwtSettings> jwtSettings, topmContext context, AWSService awsService, ApiServices apiService)
        {
            _mapper = mapper;
            _userManager = userManager;
            //_roleManager = roleManager;
            _jwtSettings = jwtSettings.Value;
            _context = context;
            _awsService = awsService;
            _apiService = apiService;
        }

        [Authorize(Roles = "admin")]
        [HttpPost("signup/admin")]
        public async Task<IActionResult> SignUpAdmin(User user)
        {
            var userCreateResult = await _userManager.CreateAsync(user, user.Pass);

            if (userCreateResult.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, "admin");
                _context.SaveChanges();
            }

            return Ok();

        }

        [HttpPost("signup/client")]
        public async Task<IActionResult> SignUpClient([FromForm] KlientSignUpResource klientSignUpResource)
        {
            var transaction = _context.Database.BeginTransaction();

            var user = _mapper.Map<UserSignUpResource, User>(klientSignUpResource.UserSignUpResource);
            var userInfo = _mapper.Map<UserInfoSignUpResource, UserInfo>(klientSignUpResource.UserInfoSignUpResource);
            var klient = _mapper.Map<KlientSignUpDataResource, Klient>(klientSignUpResource.KlientSignUpDataResource);

            var contact = _mapper.Map<ContactResource, Contact>(klientSignUpResource.UserSignUpResource.ContactResource);

            var avatar = await _awsService.UploadFileToAWSAsync(klientSignUpResource.UserInfoSignUpResource.Avatar, "avatar");

            //check avaibility city/county in DB
            var cityResource = klientSignUpResource.UserInfoSignUpResource.CityResource;
            var cityInDB = _context.City.FirstOrDefault(c => c.Name == cityResource.Name && c.Country.Name == cityResource.CountryResource.Name);

            if (cityInDB is null)
            {
                var newCity = _mapper.Map<CityResource, City>(cityResource);

                var countryInDB = _context.Country.FirstOrDefault(c => c.Name == cityResource.CountryResource.Name);

                if (countryInDB is null)
                {
                    var newCountry = _mapper.Map<CountryResource, Country>(cityResource.CountryResource);
                    newCity.Country = newCountry;
                }
                else
                {
                    newCity.CountryId = countryInDB.IdCountry;
                }

                userInfo.City = newCity;
            }
            else
            {
                userInfo.CityId = cityInDB.IdCity;
            }

            userInfo.AvatarIdAvatarNavigation = new Avatar { Avatarlink = avatar };
            userInfo.RegDate = DateTime.Now;
            user.UserInfo = userInfo;

            user.Contact = contact;

            try
            {
                var userCreateResult = await _userManager.CreateAsync(user, klientSignUpResource.UserSignUpResource.Pass);

                if (userCreateResult.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, "client");
                    klient.Id = user.Id;
                    _context.Klient.Add(klient);
                    _context.SaveChanges();
                    transaction.Commit();
                }


            }
            catch (Exception e)
            {
                return Problem(e.Message, null, 500);
            }

            return Created(string.Empty, string.Empty);

        }

        [HttpPost("signup/freelancer")]
        public async Task<IActionResult> SignUpFreelancer([FromForm] FreelancerSignUpResource freelancerSignUpResource)
        {
            var transaction = _context.Database.BeginTransaction();

            //map new user objects
            var user = _mapper.Map<UserSignUpResource, User>(freelancerSignUpResource.UserSignUpResource);
            var userInfo = _mapper.Map<UserInfoSignUpResource, UserInfo>(freelancerSignUpResource.UserInfoSignUpResource);
            var freelancer = _mapper.Map<FreelancerSignUpDataResource, Freelancer>(freelancerSignUpResource.FreelancerSignUpDataResource);

            var contact = _mapper.Map<ContactResource, Contact>(freelancerSignUpResource.UserSignUpResource.ContactResource);
            var skills = _apiService.MapSkillFreelancer(freelancerSignUpResource.SkillFreelancer);

            //upload files
            var avatar = await _awsService.UploadFileToAWSAsync(freelancerSignUpResource.UserInfoSignUpResource.Avatar, "avatar");
            var portfolio = await _awsService.UploadFileToAWSAsync(freelancerSignUpResource.FreelancerSignUpDataResource.Portfolio, "portfolio");

            //check avaibility city/county in DB
            var cityResource = freelancerSignUpResource.UserInfoSignUpResource.CityResource;
            var cityInDB = _context.City.FirstOrDefault(c => c.Name == cityResource.Name && c.Country.Name == cityResource.CountryResource.Name);

            if (cityInDB is null)
            {
                var newCity = _mapper.Map<CityResource, City>(cityResource);

                var countryInDB = _context.Country.FirstOrDefault(c => c.Name == cityResource.CountryResource.Name);

                if (countryInDB is null)
                {
                    var newCountry = _mapper.Map<CountryResource, Country>(cityResource.CountryResource);
                    newCity.Country = newCountry;
                } else
                {
                    newCity.CountryId = countryInDB.IdCountry;
                }

                userInfo.City = newCity;
            } else
            {
                userInfo.CityId = cityInDB.IdCity;
            }


            freelancer.Portfoliolink = portfolio;
            freelancer.SkillFreelancer = skills;
            userInfo.AvatarIdAvatarNavigation = new Avatar { Avatarlink = avatar };
            userInfo.RegDate = DateTime.Now;
            user.UserInfo = userInfo;

            user.Contact = contact;

            try
            {
                var userCreateResult = await _userManager.CreateAsync(user, freelancerSignUpResource.UserSignUpResource.Pass);

                if (userCreateResult.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, "freelancer");
                    freelancer.Id = user.Id;
                    _context.Freelancer.Add(freelancer);
                    _context.SaveChanges();
                    transaction.Commit();
                }
            }
            catch (Exception e)
            {
                return Problem(e.Message, null, 500);
            }

            return Created(string.Empty, string.Empty);
        }

        [HttpPost("signin")]
        public async Task<IActionResult> SignIn(UserSignInResource userSignInResource)
        {
            var user = _userManager.Users.SingleOrDefault(u => u.UserName == userSignInResource.Email);
            if (user is null)
            {
                return NotFound("User not found");
            }

            if (_userManager.GetRolesAsync(user).Result.FirstOrDefault() != "admin")
            {
                if(!user.UserInfo.IsVerified)
                return Unauthorized("User is not verified");
            }

            var userSigninResult = await _userManager.CheckPasswordAsync(user, userSignInResource.Password);

            if (userSigninResult)
            {
                var roles = await _userManager.GetRolesAsync(user);
                return Ok(GenerateJwt(user, roles));
            }

            return BadRequest("Email or password incorrect.");
        }

        [Authorize(Roles = "admin")]
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            var user = await _userManager.FindByIdAsync(id.ToString());

            if (user is null)
            {
                return NotFound("User not found");
            }

            try
            {
                var userDeleteResult = await _userManager.DeleteAsync(user);
            }

            catch (Exception e)
            {
                return Problem(e.Message, null, 500);
            }

            return Ok();
        }

        [Authorize]
        [HttpGet("current")]
        public async Task<ActionResult> GetCurrentUserRole()
        {

            var u = await _userManager.GetUserAsync(HttpContext.User);
            var role = await _userManager.GetRolesAsync(u);

            if (role.FirstOrDefault() == "admin" || u.UserInfo.IsVerified)
            {
                return Ok(new { id = u.Id, role = role.FirstOrDefault() });
            }

            return Conflict();
        }

        [Authorize(Roles = "admin")]
        [HttpGet("verify/{id:int}")]
        public async Task<ActionResult<IEnumerable<FreelancerResource>>> VerifyUser(int id)
        {
            var user = await _context.UserInfo.Where(f => f.Id == id).FirstOrDefaultAsync();

            if (user == null) return NoContent();

            user.IsVerified = true;

            _context.SaveChanges();


            return Ok();
        }
        

        private string GenerateJwt(User user, IList<string> roles)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString())
            };

            var roleClaims = roles.Select(r => new Claim(ClaimTypes.Role, r));
            claims.AddRange(roleClaims);

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtSettings.Secret));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddDays(Convert.ToDouble(_jwtSettings.ExpirationInDays));

            var token = new JwtSecurityToken(
                issuer: _jwtSettings.Issuer,
                audience: _jwtSettings.Issuer,
                claims,
                expires: expires,
                signingCredentials: creds
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }


}
