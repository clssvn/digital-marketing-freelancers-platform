﻿using System;
namespace FreelancePlatform.Resources
{
    public class UserSignInResource
    {
        public string Email { get; set; }

        public string Password { get; set; }

    }
}
