import React, { Component } from "react";
import { UserRoutingHeader } from "../../header";
import ClientCard from "../../client-card";
import { Link } from "react-router-dom";
import { fetchСlients } from "../../../actions";
import { compose } from "redux";
import { connect } from "react-redux";
import { withFreelanceAppService } from "../../hoc";
import './client-search.scss'
import Loading from "../../loading/loading";
import Error from "../../error/error";

const ClientList = ({ clients }) => {

  return Array.from(clients).map((client) => {
    return (
        <li key={client.id}>
          <Link to={`/client/id${client.id}`}>
            <ClientCard client={{client, loading: false}} />
          </Link>
        </li>
    );
  });
};

class ClientSearch extends Component {
  componentDidMount() {
    this.props.fetchСlients();
  }

  render() {

    const { clients, loading, error } = this.props.clients;

    if (loading) {
      return <Loading />;
    }

    if(error){
      return <Error />;
  }
    return (
      <>
        <UserRoutingHeader />
        <ClientList clients={clients}/>
      </>
      )
      
  }
}

const mapDispatchToProps = (dispatch, { freelanceAppService }) => {
  return {
    fetchСlients: () => fetchСlients(freelanceAppService, dispatch),
  };
};

const mapStateToProps = ({ clients }) => {
  return { clients };
};

export default compose(
  withFreelanceAppService(),
  connect(mapStateToProps, mapDispatchToProps)
)(ClientSearch);
