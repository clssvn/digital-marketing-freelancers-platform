﻿using System;
using System.Collections.Generic;

namespace FreelancePlatform.Resources
{
    public class KlientResource
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string City { get; set; }
        public DateTime RegDate { get; set; }

        public string MemberFor { get; set; }

        public string Avatar { get; set; }
        public string CompanyName { get; set; }

        public int CompanySize { get; set; }
        public string Info { get; set; }

        public NicheResource Niche { get; set; }
        public ContactResource Contact { get; set; }

    }

    public class KlientQueryResource
    {
        public int Id { get; set; }
        public string FullName { get; set; }
    }
}
