export const getUserEditData = (state, action) => {
  if (state === undefined) {
    return {
      userEditData: {},
      loading: true,
      error: null,
    };
  }

  switch (action.type) {
    case "USER_EDIT_DATA_REQUEST":
      return {
        userEditData: {},
        loading: true,
        error: null,
      };

    case "USER_EDIT_DATA_SUCCESS":
      return {
        userEditData: action.payload,
        loading: false,
        error: null,
      };

    case "USER_EDIT_DATA_FAILURE":
      return {
        userEditData: [],
        loading: false,
        error: action.payload,
      };

    default:
      return state.userEditData;
  }
};

export const getUserNewData = (state, action) => {
    if (state === undefined) {
      return {
        userNewData: {},
        loading: true,
        error: null,
      };
    }
  
    switch (action.type) {
      case "USER_NEW_DATA_REQUEST":
        return {
            userNewData: {},
          loading: true,
          error: null,
        };
  
      case "USER_NEW_DATA_SUCCESS":
        return {
            userNewData: action.payload,
          loading: false,
          error: null,
        };
  
      case "USER_NEW_DATA_FAILURE":
        return {
            userNewData: {},
          loading: false,
          error: action.payload,
        };
  
      default:
        return state.userNewData;
    }
  };