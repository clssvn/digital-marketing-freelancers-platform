import React from 'react';
import './case-card.scss';
import {
    Container,
    Row
  } from "react-bootstrap";

const CaseCard = ({project}) => {
    const { category: { name: categoryName },
    niche: { name: nicheName  }, name, description } = project;
    return(
        <Container className="case-card">

            <Row className="card-header">
                    <h6 className="case-category">{categoryName}</h6>
                    <h6 className="case-niche">{nicheName}</h6>
            </Row>
            
            <h5 className="card-title">{name}</h5>

            <h6 className="card-desc">{description}</h6>
        </Container>
    );
}

export default CaseCard; 