﻿using System;

namespace FreelancePlatform.Models
{
    public partial class UserInfo
    {
        public int Id { get; set; }
        public bool IsVerified { get; set; }
        public DateTime RegDate { get; set; }
        public string Info { get; set; }
        public int CityId { get; set; }
        public int AvatarIdAvatar { get; set; }

        public virtual Avatar AvatarIdAvatarNavigation { get; set; }
        public virtual City City { get; set; }
        public virtual User IdNavigation { get; set; }
        public virtual Freelancer Freelancer { get; set; }
        public virtual Klient Klient { get; set; }
    }
}
